This archive contains all code to rerun the experiments for the paper `The
Impact of Data Source Prediction for Input Conditioning'.

The archive is divided in the two main models used in the paper:

* [uuparser](https://github.com/UppsalaNLP/uuparser)

* [multiteamTagger](https://bitbucket.org/ahmetustunn/morphology_in_context/)

Within each of these folders, one can find a scripts/runAll.sh, which can be
used to rerun all experiments. In addition to these folders, we provide a
tables folder, in which the code to generate the tables is found (also called
runAll.sh).

These scripts assume that pytorch 1.1.0 and dynet 2.0.3 are installed.


In short, all needed to rerun all experiments and generate all tables:

```
pip3 install --user -r requirements.txt
# Run all experiments
cd uuparser
./scripts/runAll.sh
cd ../multiteamTagger
./scripts/runAll.sh
cd ../
# Generate the tables
python3 tables/filter.py
python3 tables/loo.py
python3 tables/parser.py
python3 tables/tagger.py
python3 tables/scatter.py
```

In practice, however, this will take very long. So we strongly suggest that you
parallelize the commands used in the runAll.sh scripts.


In case you are just interested in the individual scores, they can be found in:

```
uuparser/raw.dep
uuparser/raw.dep.loo (for leave-one-out)
multiteamTagger/raw.lemma
multiteamTagger/raw.morph
```

Unfortunately, we have lost the predictions of the multi-team tagger, as this project was finalized over a large time-period. The predictions of the UUParser can be found in uuparser/preds.tar.gz

