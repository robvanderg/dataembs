import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

plt.style.use('tables/css.mplstyle')

names = ['dataset', 'language', 'cluster', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']

def getData(path, idxsOrder):
    first = True
    data = []
    for line in open(path):
        if first:
            first = False
        else:
            tok = line.strip().split(', ')
            if idxsOrder == 1:
                data.append([tok[0], tok[2], tok[3], tok[4], '99.0', str(float(tok[9])*100), tok[5], tok[6], tok[7], tok[8]])
            elif idxsOrder == 2:
                data.append([tok[0], tok[0].split('_')[0], tok[1], str(float(tok[2])), tok[3], tok[8], tok[4], tok[5], tok[7], tok[6]])
    return data

markers = ["+", "o", "v"]
maxWords = 400000
yMin = -10
yMax = 25
def plot(data, name, markerIdx):
    x = []
    y = []
    for lineIdx, tok in enumerate(data):
        if lineIdx == 0:
            continue
        numWords = int(float(tok[3]))
        #if numWords > maxWords:
        #    numWords = maxWords

        goldScore = float(tok[8])
        baseScore = float(tok[6])
        scoreDiff = goldScore - baseScore
        #if scoreDiff < yMin:
        #    scoreDiff = yMin
        #if scoreDiff > yMax:
        #    scoreDiff = yMax
        if numWords > 0:
            y.append(scoreDiff)
            x.append(numWords)
        if scoreDiff < -5:
            print(name, tok[0])
    #print(x)
    #print(y)
    ax.scatter(x, y, label=name, s=100, marker=markers[markerIdx], alpha=.75, zorder=2)

depData = getData('uuparser/raw.dep', 1)
morphData = getData('multiteamTagger/raw.morph', 2)
lemmaData = getData('multiteamTagger/raw.lemma', 2)


fig, ax = plt.subplots(figsize=(8,5), dpi=300)
ax.plot([0,2000000],[0,0], color='black', zorder=1, linewidth=1)

plot(morphData, 'Morph. Tagging', 0)
plot(lemmaData, 'Lemmatization', 1)
plot(depData, 'Dep. Parsing', 2)

ax.set_ylabel('Performance increase')
ax.set_xlabel('Datasize (#words)')
leg = ax.legend(loc='upper right')
leg.get_frame().set_linewidth(1.5)
ax.set_ylim((-13,23.5))
ax.set_xlim((100,2000000))
#ax.set_xticks( [0, 100000, 200000, 300000, 400000])
#ax.set_xticklabels( ['0', '100k', '200k', '300k', '400k'])
plt.xscale("log")

fig.savefig('scatter.pdf', bbox_inches='tight')



