# TODO add word-overlap for parser and make 1/0 for gold/predicted. then put the tasks as columns

names = ['dataset', 'language', 'cluster', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']

def getData(path, idxsOrder):
    first = True
    data = []
    for line in open(path):
        if first:
            first = False
        else:
            tok = line.strip().split(', ')
            if idxsOrder == 1:
                data.append([tok[0], tok[2], tok[3], tok[4], '99.0', str(float(tok[9])*100), tok[5], tok[6], tok[7], tok[8]])
            elif idxsOrder == 2:
                data.append([tok[0], tok[0].split('_')[0], tok[1], str(float(tok[2])), tok[3], tok[8], tok[4], tok[5], tok[7], tok[6]])
    return data

print('large-highWoverlap-sameLang')
def makeTable(data, name, isDep):
    print(name)
    situations = {}

    for tok in data:
        large = int(float(tok[3])) > 100000
        highOver = float(tok[4]) > .15
        sameLang = tok[0][:2] == tok[2][:2]
        if isDep:
            for tok2 in data:
                if tok2[2] == tok[2]:
                    if tok2[1] == tok[1] and tok2[0] != tok[0]:
                        sameLang = True
        name = ''
        for val in [large, highOver, sameLang]:
            if val:
                name += '1'
            else:
                name += '0'
        if name not in situations:
            situations[name] = []
        # 9 = pred, 8 = gold
        #improve = float(tok[9]) - float(tok[6])
        improve = float(tok[8]) - float(tok[6])
        situations[name].append(improve)

    for name in sorted(situations):
        avg = sum(situations[name])/len(situations[name])
        print(name, str(len(situations[name])) + '\t{:.2f}'.format(avg))
    print()

depData = getData('uuparser/raw.dep', 1)
morphData = getData('multiteamTagger/raw.morph',2)
lemData = getData('multiteamTagger/raw.lemma',2)

makeTable(lemData, 'lemma', False)
makeTable(morphData, 'morphtag', False)
makeTable(depData, 'dependency', True)

