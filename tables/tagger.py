import sys

names = ['dataset', 'additional', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']

def getData(path):
    data = []
    first = True
    for line in open(path):
        if first:
            first = False
            continue
        tok = line.strip().split(', ')
        ordered = [tok[0], tok[1], tok[2], tok[3], tok[8], tok[4], tok[5], tok[7], tok[6]]
        ordered[0] = ordered[0].replace('_', '\\_')
        ordered[1] = ordered[1].replace('_', '\\_')
        data.append(ordered)
    return sorted(data)
data = getData('multiteamTagger/raw.morph')
data2 = getData('multiteamTagger/raw.lemma')

print('\\begin{table*}')
print('\\resizebox{.5\\textwidth}{!}{')
print('\\begin{tabular}{l l r r r | r r r r | r r r r}')
print('    \\toprule')
print('    & & & & & \\multicolumn{4}{c}{Morphological Tagging (F1)} & \\multicolumn{4}{c}{Lemmatization (Accuracy)} \\\\')
print('    ' + ' & '.join(names + names[5:9]) + ' \\\\')
print('    \\midrule')
total = [0.0] * 7
total2 = [0.0] * 4
#names are not so nice, 2 refers to second task here!
for dataset, dataset2 in zip(data, data2):
    for i in range(2,9):
        total[i-2] += float(dataset[i])
    scores = [float(x) for x in dataset[5:9]]
    for i in range(5,9):
        if float(dataset[i]) == max(scores) and dataset[i] != '-1.00':
            dataset[i] = '\\textbf{' + dataset[i] + '}'

    for i in range(5,9):
        total2[i -5] += float(dataset2[i])
    scores = [float(x) for x in dataset2[5:9]]
    for i in range(5,9):
        if float(dataset2[i]) == max(scores) and dataset2[i] != '-1.00':
            dataset2[i] = '\\textbf{' + dataset2[i] + '}'
    
    dataset[2] = '{:,}'.format(int(round(float(dataset[2]))))
    print('    '+ ' & '.join(dataset + dataset2[5:9]) + ' \\\\')

for i in range(len(total)):
    if i == 0:
        total[i] = '{:,}'.format(int(round(float(total[i]/len(data)))))
    else:
        total[i] = '{:.2f}'.format(total[i]/ len(data))
for i in range(len(total2)):
    total2[i] = '{:.2f}'.format(total2[i]/ len(data))

print('    \\midrule')
print('    ' + ' & '.join(['Avg.', ''] + total + total2) + ' \\\\')
print('    \\bottomrule')
print('\\end{tabular}}')
print('\\caption{}')
print('\\end{table*}')

