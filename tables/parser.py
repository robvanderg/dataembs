import sys
names = ['cluster', 'dataset', 'wordSize', 'svm', 'base', 'concat', 'gold', 'pred', 'concat.loo', 'pred.loo']

data = {}
first = True
for line in open('uuparser/raw.dep'):
    if first:
        first = False
        continue
    tok = line.strip().split(', ')
    ordered = [tok[3], tok[0], tok[4], tok[9], tok[5], tok[6], tok[7], tok[8]]
    cluster = tok[3]
    if cluster not in data:
        data[cluster] = {}
    for i in range(2,len(ordered)):
        ordered[i] = '{:.2f}'.format(float(ordered[i]))
    ordered[1] = ordered[1].replace('_', '\\_')
    ordered[2] = str(int(float(ordered[2])))
    data[cluster][ordered[1]] = (ordered)


looData = {}
first = True
for line in open('uuparser/raw.dep.loo'):
    if first:
        first = False
        continue
    tok = line.strip().split(', ')
    ordered = [tok[2], tok[0], '{:.2f}'.format(float(tok[3])), '{:.2f}'.format(float(tok[4]))]
    cluster = ordered[0]
    if cluster not in looData:
        looData[cluster] = {}
    #ordered[1] = ordered[1].replace('_', '\\_')
    looData[cluster][ordered[1]] = ordered

print('\\begin{table*}')
print('\\resizebox{.5\\textwidth}{!}{')
print('\\begin{tabular}{l l r r r r r r r r}')
print('    \\toprule')
print('    ' + ' & '.join(names) + ' \\\\')
total = [0.0] * 8
datasets = 0

allClusters = set()
for cluster in data:
    allClusters.add(cluster)
for cluster in looData:
    allClusters.add(cluster)

totals = [0.0] * 8
totalCounter = [0.0] * 8
for cluster in sorted(allClusters):
    print('    \\midrule')
    print (cluster, end=' ')
    allSets = set()
    for dataset in data[cluster]:
        allSets.add(dataset)
    if cluster in looData:
        for dataset in looData[cluster]:
            allSets.add(dataset)

    for dataset in allSets:
        if dataset in data[cluster]:
            firstPart = data[cluster][dataset]
            for i in range(2,8):
                totals[i-2] += float(firstPart[i])
                totalCounter[i-2] += 1
            firstPart[2] = '{:,}'.format(int(firstPart[2]))
            scores = [float(x) for x in firstPart[4:8]]
            for i in range(4,8):
                if float(firstPart[i]) == max(scores) and firstPart[i] != '-1.00':
                    firstPart[i] = '\\textbf{' + firstPart[i] + '}'
        else:
            firstPart = ['', dataset,'0','-','-','-','-','-']

        if cluster in looData and dataset in looData[cluster]:
            secondPart = looData[cluster][dataset]
            for i in range(2,4):
                totals[4+i] += float(secondPart[i])
                totalCounter[4+i] += 1
        
            scores = [float(x) for x in secondPart[2:4]]
            for i in range(2,4):
                if float(secondPart[i]) == max(scores) and secondPart[i] != '-1.00':
                    secondPart[i] = '\\textbf{' + secondPart[i] + '}'
        else:
            secondPart = ['-', '-', '-', '-']
        print('    '+ ' & '.join(['']  + firstPart[1:] + secondPart[2:]) + ' \\\\')


for i in range(len(totals)):
    total[i] = '{:.2f}'.format(totals[i]/ totalCounter[i])
    if i == 0:
        total[i] = '{:,}'.format(int(round(totals[i]/totalCounter[i])))
print('    \\midrule')
print('    ' + ' & '.join(['Average', ''] + total) + ' \\\\')
print('    \\bottomrule')
print('\\end{tabular}}')
print('\\caption{}')
print('\\end{table*}')

