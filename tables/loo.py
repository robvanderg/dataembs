import sys

names = ['cluster', 'dataset', 'concat', 'pred']


def getData(path):
    first = True
    data = []
    for line in open(path):
        if first:
            first = False
        else:
            tok = line.strip().split(', ')
            data.append([tok[2], tok[0], tok[3], tok[4], tok[5]])
    return data

def clusters(data):
    clusters = []
    for dataset in data:
        clusters.append( dataset[0])
    return clusters

def crossLing(data):
    crossLing = []
    for dataset in data:
        cross = False
        cluster = dataset[0]
        lang = dataset[1].split('\\_')[0]
        for otherItem in data:
            if otherItem[0] == cluster and otherItem[1].split('\\_')[0] != lang:
                cross = True
        crossLing.append('Cross-lang' if cross else 'Single-lang')
    return crossLing

def multiLing(data):
    crossLing = []
    for dataset in data:
        same = False
        cluster = dataset[0]
        lang = dataset[1].split('\\_')[0]
        for otherItem in data:
            
            if otherItem[0] == cluster and otherItem[1].split('\\_')[0] == lang and otherItem[1] != dataset[1]:
                same = True
        crossLing.append('$\\exists$ same-lang' if same else '$\\nexists$  same-lang')
    return crossLing

def relSizes(data):
    relLarge = []
    for dataset in data:
        large = False
        cluster = dataset[0]
        lang = dataset[1].split('\\_')[0]
        otherSize = 0
        for otherItem in data:
            if otherItem[0] == cluster:
                otherSize += int(otherItem[4])
        if int(dataset[4])/otherSize > .3333333:
            #print(dataset[1], int(dataset[4])/otherSize)
            relLarge.append('relLarge')
        else:
            relLarge.append('relSmall')
    return relLarge

def multiLing2(data):
    crossLing = []
    for dataset in data:
        same = False
        cluster = dataset[0]
        lang = dataset[1].split('\\_')[0]
        for otherItem in data:
            
            if otherItem[0] == cluster and otherItem[1].split('\\_')[0] == lang and otherItem[1] != dataset[1]:
                same = True
        cross = False
        for otherItem in data:
            if otherItem[0] == cluster and otherItem[1].split('\\_')[0] != lang:
                cross = True
        crossLing.append('Same-lang' if (same and cross) else 'not')
    return crossLing

def getAvg(cats, data):
    results = {}
    for item, cat in zip(data, cats):
        if cat not in results:
            results[cat] = []
        results[cat].append(item)
    for cat in results:
        counter = 0
        totals = [0.0] * 2
        for dataset in results[cat]:
            for i in range(2,len(dataset)-1):
                totals[i-2] += float(dataset[i])
            counter += 1
        for i in range(len(totals)):
            if counter == 0.0:
                totals[i] = 0.0
            else:
                totals[i] = totals[i] / counter
        results[cat] = [counter] + totals
    return results

def allSets(data):
    result = []
    for dataset in data:
        result.append('All')
    return result


names = ['dataset', 'language', 'cluster', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']

def combine(data, filterFunction):
    cats = filterFunction(data)
    avgs = getAvg(cats, data)
    finalResults = {}
    for cat in sorted(avgs):
        results = []
        for i in range(len(avgs[cat])):
            results.append('{:.2f}'.format(avgs[cat][i]))
        finalResults[cat] = results
    return finalResults

print('\\begin{table*}')
print('\\resizebox{\\textwidth}{!}{')
print('\\begin{tabular}{l | r | r r }')
print('    \\toprule')
print('   ' + ' & '.join(['', '\#sets', 'concat', 'pred'])+ '\\\\')


data = getData('uuparser/raw.dep.loo')

#for function in [allSets, crossLing, multiLing, multiLing2, clusters]:
for function in [allSets, crossLing, multiLing, relSizes]:
    results = combine(data, function)
    print('    \\midrule')
    for cat in results:
        if float(results[cat][1]) > float(results[cat][2]):
            results[cat][1] = '\\textbf{' + results[cat][1] + '}'
        elif float(results[cat][1]) < float(results[cat][2]):
            results[cat][2] = '\\textbf{' + results[cat][2] + '}'
        print('    ' +  ' & '.join([cat] + results[cat]) + ' \\\\') 
print('    \\bottomrule')
print('\\end{tabular}')
print('}')
print('\\caption{}')
print('\\end{table*}')
print()

