import sys

names = ['dataset', 'language', 'cluster', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']
def getData(path, idxsOrder):
    first = True
    data = []
    for line in open(path):
        if first:
            first = False
        else:
            tok = line.strip().split(', ')
            if idxsOrder == 1:
                data.append([tok[0], tok[2], tok[3], tok[4], '99.0', str(float(tok[9])*100), tok[5], tok[6], tok[7], tok[8]])
            elif idxsOrder == 2:
                data.append([tok[0], tok[0].split('_')[0], tok[1], str(float(tok[2])), tok[3], tok[8], tok[4], tok[5], tok[7], tok[6]])
    return data

def size(data):
    sizes = []
    for dataset in data:
        if int(float(dataset[3])) < 30000:
            sizes.append('Small')
        #elif int(float(dataset[3])) < 50000:
        #    sizes.append('Medium')
        else:
            sizes.append('Large')
    return sizes

def crossLing(data):
    pairs = data[0][4] != '99.0'
    crossLing = []
    if pairs:
        for dataset in data:
            cross = False
            lang1 = dataset[0].split('_')[0]
            lang2 = dataset[2].split('_')[0]
            crossLing.append('Cross-lang' if lang1 != lang2 else 'Single-lang')
    else:
        for dataset in data:
            cross = False
            cluster = dataset[2]
            lang = dataset[1]
            for otherItem in data:
                if otherItem[2] == cluster and otherItem[1] != lang:
                    cross = True
            crossLing.append('Cross-lang' if cross else 'Single-lang')
    return crossLing

def multiLing(data):
    pairs = data[0][4] != '99.0'
    crossLing = []
    if pairs:
        for dataset in data:
            cross = False
            lang1 = dataset[0].split('_')[0]
            lang2 = dataset[2].split('_')[0]
            crossLing.append('$\\nexists$ same-lang' if lang1 != lang2 else '$\\exists$ same-lang')
    else:
        for dataset in data:
            other = False
            cluster = dataset[2]
            lang = dataset[1]
            for otherItem in data:
                if otherItem[2] == cluster and otherItem[1] == lang and otherItem[0] != dataset[0]:
                    other = True

            #if other == True:
            #    print(dataset[0])
            crossLing.append('$\\exists$ same-lang' if other else '$\\nexists$ same-lang')
    return crossLing

def svmScore(data):
    pairs = data[0][4] != '99.0'
    idx = 15 if pairs else 5
    svmHigh = []
    for dataset in data:
        if len(dataset) < idx:
            idx = 5
        svmHigh.append('pred$>$95\%' if float(dataset[idx]) > 95 else 'pred$<$95\%')
    return svmHigh

def getAvg(cats, data):
    results = {}
    for item, cat in zip(data, cats):
        if cat not in results:
            results[cat] = []
        results[cat].append(item)
    for cat in results:
        counter = 0
        totals = [0.0] * 7
        for dataset in results[cat]:
            skip = False
            for i in range(5, len(dataset)):
                if float(dataset[i]) in [0.0, -1.0]:
                    skip = True
            if skip:
                #print(dataset)
                continue
            for i in range(3,len(dataset)):
                totals[i-3] += float(dataset[i])
            counter += 1
        for i in range(len(totals)):
            if counter == 0.0:
                totals[i] = 0.0
            else:
                totals[i] = totals[i] / counter
        results[cat] = [counter] + totals
    return results

def datasetIdWork(data):
    work = []
    for dataset in data:
        work.append('goldId$>$concat' if float(dataset[8]) > float(dataset[7]) else 'goldId$<$concat')
    return work

def allSets(data):
    result = []
    for dataset in data:
        result.append('All')
    return result


def overlap(data):
    result = []
    for dataset in data:
        result.append('highWO' if float(dataset[4]) > .1 else 'lowWO')
    return result

def sharedTask(data):
    result = []
    for dataset in data:
        if dataset[0] in ["bxr_bdt", "hsb_ufal", "hy_armtdp", "kk_ktb", "kmr_mg", "br_keb", "fo_oft", "pcm_nsc", "th_pud"]:
            result.append('low-res.')
        elif dataset[0] in ["gl_treegal", "ga_idt", "la_perseus", "sme_giella", "no_nynorsklia", "ru_taiga", "sl_sst"]:
            result.append('small')
        elif dataset[0] in ["cs_pud", "en_pud", "fi_pud", "ja_modern", "sv_pud"]:
            result.append('pud')
        elif dataset[0] in ["af_afribooms", "grc_perseus", "grc_proiel", "ar_padt", "eu_bdt", "bg_btb", "ca_ancora", "hr_set", "cs_cac", "cs_fictree", "cs_pdt", "da_ddt", "nl_alpino", "nl_lassysmall", "en_ewt", "en_gum", "en_lines", "et_edt", "fi_ftb", "fi_tdt", "fr_gsd", "fr_sequoia", "fr_spoken", "gl_ctg", "de_gsd", "got_proiel", "el_gdt", "he_htb", "hi_hdtb", "hu_szeged", "zh_gsd", "id_gsd", "it_isdt", "it_postwita", "ja_gsd", "ko_gsd", "ko_kaist", "la_ittb", "la_proiel", "lv_lvtb", "no_bokmaal", "no_nynorsk", "fro_srcmf", "cu_proiel", "fa_seraji", "pl_lfg", "pl_sz", "pt_bosque", "ro_rrt", "ru_syntagrus", "sr_set", "sk_snk", "sl_ssj", "es_ancora", "sv_lines", "sv_talbanken", "tr_imst", "uk_iu", "ur_udtb", "ug_udt", "vi_vtb"]:
            result.append('big')
        else:
            result.append('unk')
    return result

names = ['dataset', 'language', 'cluster', 'wordSize', 'wordOverlap', 'svm', 'base', 'concat', 'gold', 'pred']

def combine(data, filterFunction):
    cats = filterFunction(data)
    avgs = getAvg(cats, data)
    finalResults = {}
    for cat in sorted(avgs):
        results = []
        for i in range(len(avgs[cat])):
            #results.append('{:.2f}'.format(avgs[cat][i]))
            results.append(avgs[cat][i])
        #results[-1] = results[-1] - results[-4]
        #results[-2] = results[-2] - results[-4]
        #results[-3] = results[-3] - results[-4]
        for i in range(len(avgs[cat])):
            results[i] = '{:.2f}'.format(results[i])
        results[0] = str(int(float(results[0])))
        results[1] = str(int(float(results[1])))
        results[1] = '{:,}'.format(int(results[1]))

        scores = [float(x) for x in results[4:]]
        #for i in range(len(scores)):
        #    if scores[i] == max(scores):
        #        results[4+i] = '\\textbf{' + results[4+i] + '}'

        finalResults[cat] = results
    return finalResults
print('\\begin{table*}')
print('\\resizebox{\\textwidth}{!}{')
print('\\begin{tabular}{l r r r r | r r r r | r r r r | | r r r | r r r r}')
print('    \\toprule')
#print('   ' + ' & '.join(['', 'numSets'] + first[4:])+ '\\\\')


depData = getData('uuparser/raw.dep', 1)
morphData = getData('multiteamTagger/raw.morph', 2)
lemmaData = getData('multiteamTagger/raw.lemma', 2)

print( '  & \\multicolumn{4}{c}{Language Pairs} & \\multicolumn{4}{c}{Morphological Tagging (F1)} & \\multicolumn{4}{c||}{Lemmatization (Accuracy)} & \\multicolumn{3}{c}{Language Clusters} & \\multicolumn{4}{c}{Dependency Parsing (LAS)} \\\\')
print( 'Filtering & \\#sets & size & WO & svm & base & concat & gold & pred & base & concat & gold & pred & \#sets & size & svm & base & concat & gold & pred \\\\')
#for function in [allSets, size, crossLing, svmScore, datasetIdWork, overlap, sharedTask]:
for function in [allSets, size, crossLing, multiLing, svmScore, overlap]:
    line = ''
    morphResults = combine(morphData, function)
    lemmaResults = combine(lemmaData, function)
    depResults = combine(depData, function)
    #Idx in results are different compared to *Data
    #0: dataset
    #1: size in words
    #2: word overlap
    #3: svm
    #4: base, concat, gold, pred
    print('    \\midrule')
    for cat in sorted(morphResults):
        #language clusters
        combination = [cat] + morphResults[cat][0:4]
        # morph
        #print(morphResults[cat])
        combination += morphResults[cat][4:]

        # lemma
        combination += lemmaResults[cat][4:]

        # pairs
        if cat in depResults:
            combination += depResults[cat][0:2] + [depResults[cat][3]]
            # dependency
            combination += depResults[cat][4:]

        # round wordsize to 1000's
        combination[2] = str(int(round(float(combination[2].replace(',',''))/1000 )))
        if len(combination) > 13:
            combination[14] = str(int(round(float(combination[14].replace(',',''))/1000 )))

        # round svm scores
        combination[4] = '{:.2f}'.format(float(combination[4])/100)

        if len(combination) > 13:
            combination[15] = '{:.2f}'.format(float(combination[15])/100)

        print('    ' +  ' & '.join(combination) + ' \\\\') 

print('    \\bottomrule')
print('\\end{tabular}')
print('}')
print('\\caption{}')
print('\\end{table*}')
print()

