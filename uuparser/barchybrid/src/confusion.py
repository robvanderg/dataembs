# Usage: 
# python3 src/confusion.py | grep -v "^No" | grep -v "^Word" | sed "s;_;\\\_;g"

import pickle
from arc_hybrid import ArcHybridLSTM as Parser
import math
from sklearn.metrics.pairwise import cosine_similarity
from scipy import spatial

def load(path, cluster):
    stored_vocab, stored_opt = pickle.load(open(path + '/params.pickle', 'rb'))
    parser = Parser(stored_vocab, stored_opt)
    model = path + '/barchybrid.model'
    parser.Load(model)
    data = []
    #print(parser.feature_extractor.treebank_lookup)
    #print(parser.feature_extractor.treebanks)
    
    for treebank in sorted(parser.feature_extractor.treebanks):
        internalId = parser.feature_extractor.treebanks[treebank]
        vector = parser.feature_extractor.treebank_lookup[internalId].value()
        data.append((treebank, vector))
    return data

def cosine(a, b):
    sumAB = 0
    sumA = 0
    sumB = 0
    for aVal, bVal in zip(a,b):
        sumAB += aVal * bVal
        sumA += aVal * aVal
        sumB += bVal * bVal
    return 1 - sumAB/(math.sqrt(sumA)*math.sqrt(sumB))
    #return 1- abs(sumAB/(math.sqrt(sumA)*math.sqrt(sumB)))


clusters = ['af-de-nl', 'en', 'finno', 'n-ger', 'old', 'sw-sla', 'turkic', 'w-sla']
#plt.subplots(figsize=(8,5), dpi=600)

for cluster in clusters: 
    tableData = {}
    for mode in ['golds', 'preds']:
        path = '../out/datasetId.' + mode + '/' + cluster + '.' + mode[:-1] + '.1/'
        data = load(path, cluster)
        # inefficient, calculates every distance twice
        for treebank1idx in range(len(data)):
            for treebank2idx in range(len(data)):
                cosine_distance = cosine(data[treebank1idx][1], data[treebank2idx][1])
                treebank1 = data[treebank1idx][0]
                treebank2 = data[treebank2idx][0]
                #print(mode, treebank1, treebank2, cosine_distance)
                if treebank1 not in tableData:
                    tableData[treebank1] = {}
                if mode == 'golds':
                    tableData[treebank1][treebank2] = [cosine_distance, 0]
                else:
                    tableData[treebank1][treebank2][1] = cosine_distance
            #treebanks.append(treebank)
            #embeddings.append(embedding)
    #this could probably be included in the previous for loops somehow
    print()
    print('\\begin{tabular}{l |r r r r r r r}')
    print('\\toprule')
    print(' & '.join([cluster] + sorted(list(tableData))) + '\\\\')
    print('\\midrule')
    for treebank1idx in range(len(tableData)):
        data = []
        for treebank2idx in range(len(tableData)):
            treebank1 = sorted(list(tableData))[treebank1idx]
            treebank2 = sorted(list(tableData))[treebank2idx]
            #if treebank1idx < treebank2idx:
            #    data.append(tableData[treebank1][treebank2][0] - tableData[treebank1][treebank2][1])
            #else:
            #    data.append(0.0)
            if treebank1idx < treebank2idx: #gold
                data.append(tableData[treebank1][treebank2][0])
            else: #pred
                data.append(tableData[treebank1][treebank2][1])
        print(' & '.join([treebank1] + ['{:.2f}'.format(x) for x in data]) + '\\\\')
    print('\\bottomrule')
    print('\\end{tabular}')

