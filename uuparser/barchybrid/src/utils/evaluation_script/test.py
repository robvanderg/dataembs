import conll17_ud_eval
import sys

class fakeFile:
    def __init__(self, text):
        self.text = text
        self.idx = 0
    def readline(self):
        self.idx += 1
        if self.idx <= len(self.text):
            return self.text[self.idx-1]
        else:
            return None

data = []
for line in open(sys.argv[1]):
    if line[0] == '#':
        continue
    elif len(line) < 3:
        break
    else:
        data.append(line)
data.append('\n')

conll17_ud_eval.load_conllu(fakeFile(data))


