from optparse import OptionParser, OptionGroup
from options_manager import OptionsManager
import pickle, utils, os, time, sys, copy, itertools, re, random
from shutil import copyfile


def run(modeldir):
    from arc_hybrid import ArcHybridLSTM as Parser
    params = modeldir +  '/params.pickle'
    print('Reading params from ' + params)
    with open(params, 'rb') as paramsfp:
        stored_vocab, stored_opt = pickle.load(paramsfp)
        parser = Parser(stored_vocab, stored_opt)
        model = modeldir + '/barchybrid.model'
        parser.Load(model)

        outFile = open('test2', 'w')
        for treebank in sorted(parser.feature_extractor.treebanks):
            internalId = parser.feature_extractor.treebanks[treebank]
            vector = parser.feature_extractor.treebank_lookup[internalId].value()
            outFile.write(treebank + '\t' + ','.join([str(x) for x in vector]) + '\n')
        outFile.close()
        exit()

if __name__ == '__main__':
    run('../out/datasetId.golds/n-ger.gold.1/')

