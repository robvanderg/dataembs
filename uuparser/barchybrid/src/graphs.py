from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import pickle
from arc_hybrid import ArcHybridLSTM as Parser

def load(path, cluster):
    stored_vocab, stored_opt = pickle.load(open(path + 'params.pickle', 'rb'))
    parser = Parser(stored_vocab, stored_opt)
    model = path + '/barchybrid.model'
    parser.Load(model)
    datas = []
    #print(parser.feature_extractor.treebank_lookup)
    #print(parser.feature_extractor.treebanks)
    
    for treebank in sorted(parser.feature_extractor.treebanks):
        internalId = parser.feature_extractor.treebanks[treebank]
        vector = parser.feature_extractor.treebank_lookup[internalId].value()
        datas.append((treebank, vector))
        print(treebank, internalId, parser.feature_extractor.treebanks)
    return datas


clusters = ['af-de-nl', 'en', 'finno', 'n-ger', 'old', 'sw-sla', 'turkic', 'w-sla']
for mode in ['golds', 'preds']:
    index = 1
    #plt.subplots(figsize=(8,5), dpi=600)
    for cluster in clusters: 
        path = '../out/datasetId.' + mode + '/' + cluster + '.' + mode[:-1] + '.1/'
        print(cluster, mode)
        treebanks = []
        embeddings = []
        for treebank, embedding in load(path, cluster):
            treebanks.append(treebank)
            embeddings.append(embedding)

        pca = PCA(n_components=2)
        pca_result = pca.fit_transform(embeddings)
        tsne = TSNE(n_components=2)
        tsne_result = tsne.fit_transform(embeddings)

        plt.subplot(4, 2, index)
        plt.subplots_adjust(hspace=.7)
        index += 1
        for treebank, embedding in zip(treebanks, pca_result):
            plt.scatter([embedding[0]], [embedding[1]], label=treebank)
            plt.annotate(treebank, (embedding[0], embedding[1]))
        plt.title(cluster)
    plt.savefig(mode + '.pdf')
    plt.clf()

