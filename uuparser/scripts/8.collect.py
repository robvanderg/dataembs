import json
import os
import myutils

clusters, names = myutils.getClusters(False)

def getAvg(results):
    total = 0.0 
    if len(results) == 0:
        return total
    for score in results:
        total += score
    return total / len(results)

def getScore(dataset, otherName, concat):
    results = []
    for seed in myutils.seeds:
        resultFile = 'out/loo/' + dataset + '.realDev.pred.' + str(seed) + ('.concat.out' if concat else '.out')
        if os.path.isfile(resultFile):
            for line in open(resultFile):
                if line.startswith('Obtained'):
                    results.append(float(line.split(' ')[5]))
        else:
            resultFile = 'out/test/' + otherName + '-ud-test.conllu.' + str(seed) + '.'
            resultFile += 'concat' if concat else 'dataId'
            if os.path.isfile(resultFile):
                for line in open(resultFile):
                    if line.startswith('Obtained'):
                        results.append(float(line.split(' ')[5]))
            else:
                print('NF', resultFile)
    #return '{:.2f}'.format(getAvg(results))
    return getAvg(results)

def getWordSize(dataset):
    count = 0
    trainFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-train.conllu'
    if not os.path.exists(trainFile):
        return count
    for line in open(trainFile):
        if line[0].isdigit():
            count += 1
    return count

print(', '.join(['name', 'lang', 'cluster', 'concat', 'pred']))
for cluster in sorted(clusters):
    if len(clusters[cluster]) <= 2:
        continue
    for tgtDatasetIdx, tgtDataset in enumerate(clusters[cluster]):
        baseScore = getScore(tgtDataset, tgtDataset, True)
        predIdScore = getScore(tgtDataset, tgtDataset, False)
        wordSize = getWordSize(tgtDataset)
        print(', '.join([tgtDataset.replace('_', '\\_'), tgtDataset.split('_')[0], cluster] + ['{:.2f}'.format(x) for x in [baseScore, predIdScore]] + [str(wordSize)]))

