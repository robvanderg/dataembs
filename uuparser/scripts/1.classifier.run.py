import os

for trainFile in os.listdir('out/clusters'):
    if not trainFile.endswith('train.conllu'):
        continue
    devFile = trainFile.replace('train.conllu', 'dev.conllu')
    
    cmd = 'nice -n 10 python3 scripts/classifier.py --train out/clusters/' + trainFile + ' --outFile out/clusters/' + trainFile + '.pred --conll &> out/clusters/' + trainFile + '.out'
    print(cmd)
    cmd = 'nice -n 10 python3 scripts/classifier.py --train out/clusters/' + trainFile + ' --dev out/clusters/' + devFile + ' --outFile out/clusters/' + devFile + '.pred --conll &> out/clusters/' + devFile + '.out'
    print(cmd)
