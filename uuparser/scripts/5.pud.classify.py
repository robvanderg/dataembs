import os
import json
import myutils

clusters, names = myutils.getClusters(False)


tgtDir = 'out/test/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for cluster in clusters:
    if len(clusters[cluster]) == 1:
        continue
    for datasetIdx, dataset in enumerate(sorted(clusters[cluster])):
        train, dev, test = myutils.getTrainDevTest(names[dataset])
        if train == '':
            #print(dataset, cluster)
            #print(test)
            cmd = 'python3 scripts/classifier.py --train out/clusters/' + cluster + '.train.conllu --dev ' + test 
            test = test.split('/')[-1]
            cmd += ' --outFile ' + tgtDir + test + ' --conll &> '+ tgtDir + test + '.out'
            print(cmd)

