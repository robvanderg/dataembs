from sklearn.svm import LinearSVC
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.pipeline import FeatureUnion
from sklearn.metrics import confusion_matrix
from optparse import OptionParser
import random
import os

random.seed='8446'

def getMisc(string):
    result = {}
    for item in string.strip().split('|'):
        if '=' in item:
            result[item.split('=')[0]] = item.split('=')[1]
    if "goldDatasetId" not in result:
        print("error, goldDatasetId not found:", string)
        exit(1)
    return result

def readData(path, shuffle=False, conll=False):
    data = []
    curSent = []
    curGold = ''
    curTags = []
    curIdx = 0
    for line in open(path):
        if conll and line[0] == '#':
            continue #remove comments for now
        tok = line.strip().split('\t')
        if (len(tok) != 3 and not conll) or (len(tok) != 10 and conll):
            data.append([' '.join(curSent), curGold, curTags, curIdx])
            curIdx += 1
            curSent = []
            curTags = []
            curGold = ''
        else:
            curSent.append(tok[0 if not conll else 1])
            curGold = tok[2] if not conll else getMisc(tok[-1])['goldDatasetId']
            curTags.append(tok[1] if not conll else tok)
    if shuffle:
        random.shuffle(data)
    textData = []
    gold = []
    tags = []
    idxs = []
    for item in data:
        textData.append(item[0])
        gold.append(item[1])
        tags.append(item[2])
        idxs.append(item[3])
    return textData, gold, tags, idxs

def tok(x):
    return x.split(' ')

def identity(x):
    return x

def run(trainData, trainGold, devData, wordNgr, charNgr):
    print('Calculating features')
    wordNgr = [int(x) for x in wordNgr.split('-')]
    charNgr = [int(x) for x in charNgr.split('-')]
    wordVectorizer = TfidfVectorizer(strip_accents=identity, lowercase=identity,
                preprocessor=identity, tokenizer=tok, analyzer='word',
                ngram_range=wordNgr)
    charVectorizer = TfidfVectorizer(strip_accents=identity, lowercase=identity,
                preprocessor=identity, tokenizer=identity, analyzer='char',
                ngram_range=charNgr)
    vectorizer = FeatureUnion([('word', wordVectorizer), ('char', charVectorizer)])
    trainFeats = vectorizer.fit_transform(trainData)
    
    print('Training')
    classifier = LinearSVC()
    classifier.fit(trainFeats, trainGold)

    print('Predicting')
    devFeats = vectorizer.transform(devData)
    return classifier.predict(devFeats)
    
def runKfold(trainData, trainGold, wordNgr, charNgr, splits):
    allPreds = []
    for i in range(splits):
        print("Processing fold " + str(i))
        beg = int(len(trainData) * (1/splits) * i)
        end = int(len(trainData) * (1/splits) * (i + 1))
        foldDevData = trainData[beg:end]
        foldTrainData = trainData[:beg] + trainData[end:]
        foldTrainGold = trainGold[:beg] + trainGold[end:] 
        allPreds += list(run(foldTrainData, foldTrainGold, foldDevData, wordNgr, charNgr))
    return allPreds

def report(preds, devGold):
    cor = total = 0
    for i in range(len(preds)):
        if preds[i] == devGold[i]:
            cor += 1
        total += 1
    #predicted is on top, gold = on the left
    print(confusion_matrix(devGold, preds))
    #print(preds)
    print(cor/total)
    print(cor, total)

def addMisc(tags, newTag):
    if tags == '_':
        return 'predDatasetId=' + newTag
    else:
        return tags + '|predDatasetId=' + newTag

def writeConl(pred, tags, path):
    outFile = open(path, 'w')
    for sentIdx in range(len(tags)):
        for wordIdx in range(len(tags[sentIdx])):
            tags[sentIdx][wordIdx][-1] = addMisc(tags[sentIdx][wordIdx][-1], str(pred[sentIdx]))
            outFile.write('\t'.join(tags[sentIdx][wordIdx]) + '\n')
        outFile.write('\n')
    outFile.close()
    
def writeData(data, pred, gold, tags, path, conll=False, idxs=None):
    if idxs != None:
        newIdxs = [None] * len(idxs)
        for i in range(len(idxs)):
            newIdxs[idxs[i]] =i
        newData = []
        newPred = []
        newGold = []
        newTags = []
        print(data[newIdxs[0]])
        for i in newIdxs:
            newData.append(data[i])
            newPred.append(pred[i])
            newGold.append(gold[i])
            newTags.append(tags[i])
        data = newData
        pred = newPred
        gold = newGold
        tags = newTags
    if conll:
        writeConl(pred, tags, path)
        return
    outFile = open(path, 'w')
    for i in range(len(data)):
        words = data[i].split(' ')
        for j in range(len(words)):
            outFile.write(words[j] + '\t' + tags[i][j] + '\t' + str(gold[i]) + '\t' + str(pred[i]) + '\n')
        outFile.write('\n')
    outFile.close()
    

if __name__ == '__main__':
    parser = OptionParser(description='Classifier on the sentence level')
    parser.add_option('--train', default='', help='path to train file')
    parser.add_option('--dev', default='', help='path to dev file. If not specified 5-fold is used on train')
    parser.add_option('--wordNgr', default='1-2', help='word n-grams range to use')
    parser.add_option('--charNgr', default='1-5', help='character n-grams range to use')
    parser.add_option('--outFile', default='', help='file to write prediction')
    parser.add_option('--conll', default=False, action='store_true', help='use connlu file format, expects goldDatasetId=[0-9]* in misc column')
    (opts, args) = parser.parse_args()
    
    if opts.train == '':
        print('please provide --train')
        exit(0)

    if opts.dev != '':
        print('Reading data')
        if not os.path.isfile(opts.dev):
            print('dev does not exist: '  + opts.dev)
            exit(1)
        trainData, trainGold, trainTags, _ = readData(opts.train, shuffle=True, conll=opts.conll)
        devData, devGold, devTags, _ = readData(opts.dev, shuffle=False, conll=opts.conll)
        preds = run(trainData, trainGold, devData, opts.wordNgr, opts.charNgr)
        report(preds, devGold)
        if opts.outFile != '':
            writeData(devData, preds, devGold, devTags, opts.outFile, conll=opts.conll)
    else:
        #TODO is it bad that trainData is shuffled?
        print('Reading data')
        trainData, trainGold, trainTags, trainIdxs = readData(opts.train, shuffle=True, conll=opts.conll)
        preds = runKfold(trainData, trainGold, opts.wordNgr, opts.charNgr, 5)
        report(preds, trainGold)
        if opts.outFile != '':
            writeData(trainData, preds, trainGold, trainTags, opts.outFile, conll=opts.conll, idxs=trainIdxs)


