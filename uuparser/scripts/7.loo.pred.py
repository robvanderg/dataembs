import json
import os
import myutils

clusters, names = myutils.getClusters(True)

for cluster in sorted(clusters):
    if len(clusters[cluster]) <= 2:
        continue
    for dataset in sorted(clusters[cluster]):
        for seed in myutils.seeds:
            realDev = '../out/loo/' + dataset + '.realDev.pred'
            if not os.path.isfile(realDev[3:]):
                continue
            modelDir = '../out/loo/' + dataset + '.model.' + str(seed) + '/'
            #if os.path.isfile(modelDir[3:] + 'barchybrid.model'):
            cmd = 'cd barchybrid && python3 src/parser.py  --multiling --datasetIdsFromFile pred  --outdir ' + modelDir + ' --testfile ' + realDev + ' --predict --dynet-seed ' + str(seed) + ' > ' + realDev + '.' + str(seed) + '.out  && cd ../'
            print(cmd)

            modelDir2 = modelDir[:-1] + '.concat' 
            #if os.path.isfile(modelDir2[3:] + 'barchybrid.model'):
            cmd = 'cd barchybrid && python3 src/parser.py --outdir ' + modelDir2 + ' --testfile ' + realDev + ' --predict --dynet-seed ' + str(seed) + ' > ' + realDev + '.' + str(seed) + '.concat.out && cd ../'
            print(cmd)

