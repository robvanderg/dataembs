import json
import os
import myutils

def readConl(path, datasetId):
    data = []
    #TODO keep words with [.-] in idx?
    if not os.path.isfile(path):
        return []
    for line in open(path):
        tok = line.strip().split('\t')
        if line.startswith('#'):
            data.append(line)
        elif len(tok) == 10:
            tok[-1] = myutils.addMisc(tok[-1], str(datasetId))
            data.append('\t'.join(tok) + '\n')
        else:
            data.append('\n')
    return data

clusters, names = myutils.getClusters(True)

tgtDir = 'out/loo/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for cluster in clusters:
    if len(clusters[cluster]) <= 2:
        continue
    #prepare real dev
    for tgtDatasetIdx, tgtDataset in enumerate(clusters[cluster]):
        trainFile, devFile, testFile = myutils.getTrainDevTest(names[tgtDataset])
        outFile = open(tgtDir + tgtDataset + '.realDev', 'w')
        for line in readConl(devFile if os.path.isfile(devFile) else testFile, tgtDatasetIdx):
            outFile.write(line)
        outFile.close()

    #prepare train/dev/test
    for split in ['train', 'dev', 'test']:
        for tgtDatasetIdx, tgtDataset in enumerate(clusters[cluster]):
            outFile = open(tgtDir + tgtDataset + '.' + split, 'w')
            for trainDatasetIdx, trainDataset in enumerate(clusters[cluster]):
                trainFile, devFile, testFile = myutils.getTrainDevTest(names[trainDataset])
                splitPaths = {'train':trainFile, 'dev':devFile, 'test':testFile}
                if trainDatasetIdx == tgtDatasetIdx:
                    continue
                for line in readConl(splitPaths[split], trainDatasetIdx):
                    outFile.write(line)
            outFile.close()


