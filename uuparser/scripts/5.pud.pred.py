import json
import os
import myutils

clusters, names = myutils.getClusters(False)

tgtDir = 'out/test/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for cluster in clusters:
    if len(clusters[cluster]) == 1:
        continue
    for datasetIdx, dataset in enumerate(sorted(clusters[cluster])):
        train, dev, test = myutils.getTrainDevTest(names[dataset])
        test = test.split('/')[-1]
        if train == '':
            for seed in myutils.seeds:
                # pred dataset ID
                testFile = '../' + tgtDir + test
                modelDir = '../out/datasetId.preds/' + cluster + '.pred.' + str(seed)
                cmd = 'cd barchybrid && python3 src/parser.py --multiling --outdir ' + modelDir + ' --testfile ' + testFile + ' --predict > ' + testFile + '.' + str(seed) + '.dataId && cd ../'
                print(cmd)

                # concat 
                modelDir = '../out/concat/' + cluster + '.' + str(seed)
                cmd = 'cd barchybrid && python3 src/parser.py --outdir ' + modelDir + ' --testfile ' + testFile + ' --predict > ' + testFile + '.' + str(seed) + '.concat && cd ../'
                print(cmd)
                


