import json
import os
import myutils


clusters, names  = myutils.getClusters(True)

dataDir = 'out/loo/'

for cluster in clusters:
    if len(clusters[cluster]) <= 2:
        continue
    for datasetIdx, dataset in enumerate(sorted(clusters[cluster])):
        trainFile = dataDir + dataset + '.train'
        devFile = dataDir + dataset + '.dev'
        realDevFile = dataDir + dataset + '.realDev'
        
        cmd = 'nice -n 10 python3 scripts/classifier.py --train ' + trainFile + ' --dev ' + devFile + ' --outFile ' + devFile + '.pred --conll &> ' + devFile + '.out'
        print(cmd)
        
        cmd = 'nice -n 10 python3 scripts/classifier.py --train ' + trainFile + ' --outFile ' + trainFile + '.pred --conll &> ' + trainFile + '.out'
        print(cmd)

        if os.path.isfile(realDevFile):
            cmd = 'nice -n 10 python3 scripts/classifier.py --train ' + trainFile + ' --dev ' + realDevFile + ' --outFile ' + realDevFile + '.pred --conll &> ' + realDevFile + '.out'
            print(cmd)

