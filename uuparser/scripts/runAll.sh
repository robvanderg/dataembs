./scripts/0.getData.sh

python3 scripts/1.classifier.prep.py
python3 scripts/1.classifier.run.py > 1.run.sh
chmod +x 1.run.sh
./1.run.sh

python3 scripts/2.base.train.py > 2.run.sh
chmod +x 2.run.sh
./2.run.sh

python3 scripts/3.concat.train.py > 3.run.sh
chmod +x 3.run.sh
./3.run.sh

python3 scripts/4.datasetId.train.py > 4.run.sh
chmod +x 4.run.sh
./4.run.sh

python3 scripts/5.pud.classify.py > 5.run.sh
chmod +x 5.run.sh
./5.run.sh

python3 scripts/5.pud.tag.py > 5.tag.sh
chmod +x 5.tag.sh
./5.tag.sh

python3 scripts/6.collect.py > raw.dep

python3 scripts/7.loo.prep.py
python3 scripts/7.loo.svm.py > 7.svm.sh
chmod +x 7.svm.sh
./7.svm.sh
python3 scripts/7.loo.train.py > 7.train.sh
chmod +x 7.train.sh
./7.train.sh
python3 scripts/7.loo.pred.py > 7.pred.sh
chmod +x 7.pred.sh
./7.pred.sh

python3 scripts/8.collect.py > raw.dep.loo



