import json
import os
import myutils

clusters, names = myutils.getClusters(True)

dataDir = 'out/loo/'

if not os.path.isdir(dataDir):
    os.mkdir(dataDir)

for seed in myutils.seeds:
    for cluster in clusters:
        #if cluster != 'n-ger' or seed != 1:
        #    continue
        if len(clusters[cluster]) <= 2:
            continue
        for datasetIdx, dataset in enumerate(sorted(clusters[cluster])):
            trainFile = '../' + dataDir + dataset + '.train.pred'
            devFile = '../' + dataDir + dataset + '.dev.pred'
            outDir = '../' + dataDir + dataset + '.model.' + str(seed)
            cmd = 'cd barchybrid && nice -n 10 python3 src/parser.py --multiling --datasetIdsFromFile pred --trainfile ' + trainFile + ' --devfile ' + devFile + ' --outdir ' + outDir + ' --max-sentence 15000 --dynet-seed ' + str(seed) + ' &> ' + outDir + '.out && cd ../'
            #if not os.path.isfile(outDir[3:] + '/barchybrid.model2'):
            print(cmd)
            outDir += '.concat'
            cmd = 'cd barchybrid && nice -n 10 python3 src/parser.py --trainfile ' + trainFile + ' --devfile ' + devFile + ' --outdir ' + outDir + ' --max-sentence 15000 --dynet-seed ' + str(seed) + ' &> ' + outDir + '.out && cd ../'
            #if not os.path.isfile(outDir[3:] + '/barchybrid.model2'):
            print(cmd)
