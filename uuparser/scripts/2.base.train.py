import json
import os
import myutils

seeds = myutils.seeds
clusters, names = myutils.getClusters(True)

tgtDir = 'out/base.preds/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for seed in myutils.seeds:
    for cluster in clusters:
        if len(clusters[cluster]) == 1:
            continue
        for dataset in sorted(clusters[cluster]):
            train, dev, test = myutils.getTrainDevTest(names[dataset])
            if train == '':
                continue
            outDir = '../' + tgtDir + dataset + '.'+ str(seed) 
            cmd = 'cd barchybrid && nice -n 10 python3 src/parser.py --outdir ' + outDir + '/'
            cmd += ' --trainfile ../' + train
            cmd += ' --devfile ../' + (dev if dev != '' else test)
            cmd += ' --max-sentence 15000 --dynet-seed ' + str(seed)
            cmd += ' &> ' + outDir + '.out && cd ..'
            if not os.path.isfile(outDir[3:] + '/best_dev_epoch.txt'):
                print(cmd)


