import os
import json

seeds = [1,2,3]
UDdir = 'ud-treebanks-v2.2/'

def getTrainDevTest(dataset):
    trainFile = ''
    devFile = ''
    testFile = ''
    for conlFile in os.listdir(UDdir + dataset):
        if conlFile.endswith('train.conllu'):
            trainFile = UDdir + dataset + '/' + conlFile
        if conlFile.endswith('dev.conllu'):
            devFile = UDdir + dataset + '/' + conlFile
        if conlFile.endswith('test.conllu'):
            testFile = UDdir + dataset + '/' + conlFile
    return trainFile, devFile, testFile

#TODO add getMisc
#also getData, readConl, getAvg, getScore?

def getClusters(exlTrain):
    names = {}
    clusters = {}
    data = json.load(open('barchybrid/src/utils/ud2.2_shared_task_models-v2.json'))
    for dataset in sorted(data):
        if exlTrain and not os.path.isfile('ud-treebanks-v2.2/' + dataset + '/' + data[dataset][0] + '-ud-train.conllu'):
            continue
        cluster = data[dataset][2]
        if cluster not in clusters:
            clusters[cluster] = []
        clusters[cluster].append(data[dataset][0])
        names[data[dataset][0]] = dataset
    return clusters, names

def addMisc(tags, newTag):
    if tags == '_':
        return 'goldDatasetId=' + newTag
    else:
        return tags + '|goldDatasetId=' + newTag

