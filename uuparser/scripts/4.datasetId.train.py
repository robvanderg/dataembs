import json
import os
import myutils

tgtDir = 'out/datasetId.preds/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)
if not os.path.isdir(tgtDir.replace('pred', 'gold')):
    os.mkdir(tgtDir.replace('pred', 'gold'))

for seed in myutils.seeds:
    for trainFile in os.listdir('out/clusters/'):
        if not trainFile.endswith('train.conllu.pred'):
            continue
        seed = str(seed)
        outDir = '../' + tgtDir + trainFile.replace('.train.conllu.pred', '') + '.pred.' + seed
        trainFile = '../out/clusters/' + trainFile
        devFile = trainFile.replace('train.conllu', 'dev.conllu')
        cmd = 'cd barchybrid && nice -n 10 python3 src/parser.py --multiling --trainfile ' + trainFile + ' --devfile ' + devFile + ' --outdir ' + outDir + ' --max-sentences 15000 --datasetIdsFromFile pred --dynet-seed ' + seed + ' &> ' + outDir + '.out && cd ../'
        #if not os.path.isdir(outDir[3:]):
        print(cmd)
        cmd = cmd.replace('pred', 'gold')
        cmd = cmd.replace('train.conllu.gold', 'train.conllu.pred')
        cmd = cmd.replace('test.conllu.gold', 'test.conllu.pred')
        cmd = cmd.replace('dev.conllu.gold', 'dev.conllu.pred')
        #if not os.path.isdir(outDir.replace('pred', 'gold')[3:]):
        print(cmd)


