import os
import json
import myutils

clusters, names = myutils.getClusters(False)

def avg(floatList):
    total = 0.0
    if len(floatList) == 0:
        return total
    for item in floatList:
        total += item
    return total/len(floatList)

def getBaseScore(dataset, folder):
    scores = []
    for seed in myutils.seeds:
        score = -1.0
        scoreFile = 'out/' + folder + '/' + dataset + '.' + str(seed) + '/best_dev_epoch.txt'
        if os.path.isfile(scoreFile):
            score = float(open(scoreFile).readline().split()[3])
        if score == -1.0:
            scoreFile = scoreFile.replace('best_dev_epoch.txt', 'out')
            if os.path.isfile(scoreFile):
                for line in open(scoreFile):
                    if line.startswith('Obtained'):
                        score = float(line.split(' ')[5])
        scores.append(score)
    return avg(scores)

def getMisc(string):
    result = {}
    string = string.strip().split('\t')[-1]
    for item in string.strip().split('|'):
        if '=' in item:
            result[item.split('=')[0]] = item.split('=')[1]
    return result

def getIdxScore(predFile, goldFile, dataset):
    tmpPred = 'pred.tmp'
    if not os.path.isfile(goldFile):
        return -1.0
    outFile = open(tmpPred, 'w')
    for line in open(predFile):
        if line[0] == '#':
            continue
        elif len(line) < 3:
            if last:
                outFile.write('\n')
        elif getMisc(line)['goldDatasetId'] == dataset:
            outFile.write(line)
            last = True
        else:
            last = False 
    outFile.close()
    #TODO replace here to test for significance
    cmd = 'python3 barchybrid/src/utils/evaluation_script/conll17_ud_eval.py ' 
    cmd += goldFile + ' ' + tmpPred + ' > ' + 'eval.tmp'
    os.system(cmd)

    result = open('eval.tmp').readline().strip().split()
    if len(result) < 2:
        return -1.0
    else:
        return float(result[-1])

def getConcatScore(dataset, cluster):
    scores = []
    for seed in myutils.seeds:
        bestFile = 'out/concat/' + cluster + '.' + str(seed) + '/best_dev_epoch.txt'
        if not os.path.isfile(bestFile):
            continue
        bestEpoch = open(bestFile).readline().strip().split()[-1]
        predFile = 'out/concat/' + cluster + '.' + str(seed) + '/out.conll'
        if not os.path.isfile(predFile):
            predFile = 'out/concat/' + cluster + '.' + str(seed) + '/dev_epoch_' + bestEpoch + '.conllu'
        goldFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-dev.conllu'
        if not os.path.isfile(goldFile):
            goldFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-test.conllu'
        scores.append(getIdxScore(predFile, goldFile, dataset))
    return avg(scores)

#TODO merge with previous
def getScore2(dataset, cluster, mode):
    scores = []
    for seed in myutils.seeds:
        bestFile = 'out/datasetId.' + mode + 's/' + cluster + '.' + mode + '.' + str(seed) + '/best_dev_epoch.txt'
        if not os.path.isfile(bestFile):
            continue
        bestEpoch = open(bestFile).readline().strip().split()[-1]
        predFile = 'out/datasetId.' + mode + 's/' + cluster + '.' + mode + '.' + str(seed) + '/dev_epoch_' + bestEpoch + '.conllu'
        if not os.path.isfile(predFile):
            predFile = 'out/datasetId.' + mode + 's/' + cluster + '.' + mode + '.' + str(seed) + '/out.conllu'

        goldFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-dev.conllu'
        if not os.path.isfile(goldFile):
            goldFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-test.conllu'
        scores.append(getIdxScore(predFile, goldFile, dataset))
    return avg(scores)

def getSVMscore(cluster):
    tp = 0
    fp = 0
    tn = 0
    fn = 0
    for line in open('out/clusters/' + cluster + '.train.conllu.pred'):
        tok = line.strip().split('\t')
        if len(tok) == 10:
            misc = getMisc(tok[-1])
            if misc['goldDatasetId'] == dataset:
                if misc['predDatasetId'] == dataset:
                    tp += 1
                else:
                    fn += 1
            else:
                if misc['predDatasetId'] == dataset:
                    fp += 1
                else:
                    tn += 1
    if tp == 0:
        return 0.0
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall)
    return f1

def getWordSize(dataset):
    count = 0
    trainFile = 'ud-treebanks-v2.2/' + names[dataset] + '/' + dataset + '-ud-train.conllu'
    if not os.path.exists(trainFile):
        return count
    for line in open(trainFile):
        if line[0].isdigit():
            count += 1
    return count

data = {}
for line in open('scripts/ids'):
    tok = line.strip().split('\t')
    cluster = tok[0] 
    if cluster not in data:
        data[cluster] = []
    data[cluster].append(tok[2])


print('dataset, names[dataset], language, cluster, wordSize, baseScore, concatScore, goldScore, predScore, svmScore')
for cluster in clusters:
    if len(clusters[cluster]) == 1:
        continue
    for dataset in clusters[cluster]:
        #if dataset != 'hsb_ufal':
        #    continue
        baseScore = getBaseScore(dataset, 'base.preds')
        concatScore = getConcatScore(dataset, cluster)
        predScore = getScore2(dataset, cluster, 'pred')
        goldScore = getScore2(dataset, cluster, 'gold')
        svmScore = getSVMscore(cluster)
        wordSize = getWordSize(dataset)
        language = dataset.split('_')[0]
        results = [dataset, names[dataset], language, cluster, wordSize, baseScore, concatScore, goldScore, predScore, svmScore]
        results = [str(result) for result in results]
        print(', '.join(results), flush=True)
        


