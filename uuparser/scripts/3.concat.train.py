import json
import os
import myutils

tgtDir = 'out/concat/'
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

for seed in myutils.seeds:
    for trainFile in os.listdir('out/clusters/'):
        if not trainFile.endswith('train.conllu.pred'):
            continue
        seed = str(seed)
        outDir = '../' + tgtDir + trainFile.replace('.train.conllu.pred', '') + '.' + seed
        trainFile = '../out/clusters/' + trainFile
        devFile = trainFile.replace('train.conllu', 'dev.conllu')
        cmd = 'cd barchybrid && nice -n 10 python3 src/parser.py --trainfile ' + trainFile + ' --devfile ' + devFile + ' --outdir ' + outDir + ' --max-sentences 15000 --dynet-seed ' + seed + ' &> ' + outDir + '.out && cd ../'
        #if not os.path.isdir(outDir[3:]):
        print(cmd)


