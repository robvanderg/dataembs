import json
import os
import myutils

def readConl(path, dataset):
    data = []
    for line in open(path):
        tok = line.strip().split('\t')
        if line.startswith('#'):
            data.append(line)
        elif len(tok) == 10:
            tok[-1] = myutils.addMisc(tok[-1], dataset)
            data.append('\t'.join(tok) + '\n')
        else:
            data.append('\n')
    return data

clusters, names = myutils.getClusters(True)

tgtDir = 'out/clusters/'
if not os.path.isdir('out'):
    os.mkdir('out')
if not os.path.isdir(tgtDir):
    os.mkdir(tgtDir)

#save for future reference
configFile = open('scripts/ids', 'w')
for cluster in clusters:
    if len(clusters[cluster]) == 1:
        continue
    for datasetIdx, dataset in enumerate(sorted(clusters[cluster])):
        configFile.write('\t'.join([cluster, str(datasetIdx), dataset]) + '\n')
configFile.close()

for cluster in clusters:
    if len(clusters[cluster]) == 1:
        continue
    print(cluster, len(clusters[cluster]))
    print(clusters[cluster])
    for split in ['train', 'dev', 'test', 'goldDev']:
        print('\n', split)
        outFile = open(tgtDir + cluster + '.' + split + '.conllu', 'w')
        for dataset in clusters[cluster]:
            print(dataset)
            trainFile, devFile, testFile = myutils.getTrainDevTest(names[dataset])
            splitPaths = {'train':trainFile, 'dev':devFile, 'test':testFile, 'goldDev':devFile}
            if (split == 'dev' or split == 'goldDev') and not os.path.isfile(splitPaths[split]):
                splitPaths[split] = testFile
            if not os.path.isfile(splitPaths[split]):
                continue
            if split == 'goldDev' and trainFile == '':
                continue
            for line in readConl(splitPaths[split], dataset):
                outFile.write(line)
        outFile.close()



