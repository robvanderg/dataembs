import random

import torch
import torch.nn as nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from tqdm import tqdm

from utils import masked_cross_entropy


class CharEncoder(nn.Module):

    def __init__(self, embedding_size, hidden_size, char_vocab_size, dropout_ratio, device, rnn_type='gru', bi_char=False, mode='max', bidirectional=False):

        super(CharEncoder, self).__init__()

        self.bidirectional = bidirectional
        self.vocab_size = char_vocab_size
        self.dropout_ratio = dropout_ratio
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size

        self.b_dim = 2 if self.bidirectional else 1

        self.embeddings = nn.Embedding(self.vocab_size, self.embedding_size, padding_idx=0)
        if rnn_type == 'gru':
            self.rnn = nn.GRU(self.embedding_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)
        elif rnn_type == 'lstm':
            self.rnn = nn.LSTM(self.embedding_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)
        self.dropout = nn.Dropout(self.dropout_ratio)

        self.rnn_hidden = None
        self.mode = mode
        self.device = device

        self.bi_char = bi_char

    def init_hidden(self, batch_size):

        _init = torch.zeros(self.b_dim, batch_size, self.hidden_size).to(self.device)
        if isinstance(self.rnn, nn.LSTM):
            # LSTM hidden contains a tuple (hidden state, cell state)
            h = (_init, _init)
        else:
            h = _init
        return h

    def cat_directions(self, hidden):

        def _cat(h):
            return torch.cat([h[0:h.size(0):2], h[1:h.size(0):2]], 2)

        if isinstance(self.rnn, nn.LSTM):
            # LSTM hidden contains a tuple (hidden state, cell state)
            hidden = tuple([_cat(h) for h in hidden])
        else:
            # GRU hidden
            hidden = _cat(hidden)

        return hidden

    def forward(self, x):

        # Sort instances by sequence length in descending order
        seq_lens, idx_sort = torch.sort(x[1], dim=0, descending=True)
        idx_unsort = torch.argsort(idx_sort)

        char_input = x[0]
        char_input = char_input[idx_sort]

        self.rnn_hidden = self.init_hidden(char_input.size(0))

        char_embeddings = self.embeddings(char_input)
        char_embeddings = self.dropout(char_embeddings)

        packed_input = nn.utils.rnn.pack_padded_sequence(char_embeddings, seq_lens, batch_first=True)
        out, hidden = self.rnn(packed_input, self.rnn_hidden)
        out = nn.utils.rnn.pad_packed_sequence(out, batch_first=True)[0]

        # Re-order
        if self.bidirectional:
            hidden = self.cat_directions(hidden)
            if isinstance(hidden, tuple):
                hidden = tuple(h.squeeze(0)[idx_unsort] for h in hidden)
            else:
                hidden = hidden.squeeze(0)[idx_unsort]

        # unsort hidden state
        out = out[idx_unsort]
        if self.mode == 'mean':
            sent_len = x[1].float()
            emb = torch.sum(out, 1)
            emb = emb.transpose(0, 1) / sent_len
            emb = emb.transpose(0, 1)
        elif self.mode == 'max':
            out[out == 0] = -1e9
            emb = torch.max(out, 1)[0]
            if emb.ndimension() == 3:
                emb = emb.squeeze(0)
                assert emb.ndimension() == 2
        elif self.mode == 'last':
            emb = hidden

        out[out == -1e9] = 0

        hidden = self.dropout(hidden)
        out = self.dropout(out)
        emb = self.dropout(emb)

        if isinstance(hidden, tuple):
            hidden_forward = tuple(h.view(h.shape[0], self.b_dim, -1)[:, 0, :] for h in hidden)
        else:
            hidden_forward = hidden.view(hidden.shape[0], self.b_dim, -1)[:, 0, :]

        if not self.bi_char:
            hidden = hidden_forward

        return emb.view(1, *emb.shape), hidden, out


class WordEncoder(nn.Module):

    def __init__(self, embedding_size, hidden_size, word_vocab_size, dropout_ratio, device, rnn_type='gru', bidirectional=True):
        super(WordEncoder, self).__init__()

        self.bidirectional = bidirectional
        self.word_vocab_size = word_vocab_size
        self.dropout_ratio = dropout_ratio
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size
        self.rnn_type = rnn_type

        self.b_dim = 2 if self.bidirectional else 1

        self.embeddings = nn.Embedding(self.word_vocab_size, self.embedding_size, padding_idx=0)
        if rnn_type == 'gru':
            self.rnn = nn.GRU(self.embedding_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)
        elif rnn_type == 'lstm':
            self.rnn = nn.LSTM(self.embedding_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)
        self.dropout = nn.Dropout(self.dropout_ratio)

        self.rnn_hidden = None
        self.device = device

    def init_hidden(self):

        _init = torch.zeros(self.b_dim, 1, self.hidden_size).to(self.device)
        if isinstance(self.rnn, nn.LSTM):
            # LSTM hidden contains a tuple (hidden state, cell state)
            h = (_init, _init)
        else:
            h = _init
        return h

    def forward(self, x):

        self.rnn_hidden = self.init_hidden()

        word_embeddings = self.embeddings(x)
        word_embeddings = self.dropout(word_embeddings)
        out, hidden = self.rnn(word_embeddings, self.rnn_hidden)
        out = self.dropout(out)
        # hidden = self.dropout(hidden)

        return out


class SentenceEncoder(nn.Module):

    def __init__(self, input_size, hidden_size, dropout_ratio, device, rnn_type='gru', bidirectional=True):

        super(SentenceEncoder, self).__init__()

        self.bidirectional = bidirectional
        self.dropout_ratio = dropout_ratio
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.rrn_type = rnn_type

        self.b_dim = 2 if self.bidirectional else 1
        if rnn_type == 'gru':
            self.rnn = nn.GRU(self.input_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)
        elif rnn_type =='lstm':
            self.rnn = nn.LSTM(self.input_size, self.hidden_size, bidirectional=bidirectional, batch_first=True)

        self.dropout = nn.Dropout(self.dropout_ratio)

        self.rnn_hidden = None
        self.device = device

    def init_hidden(self):

        _init = torch.zeros(self.b_dim, 1, self.hidden_size).to(self.device)
        if isinstance(self.rnn, nn.LSTM):
            # LSTM hidden contains a tuple (hidden state, cell state)
            h = (_init, _init)
        else:
            h = _init
        return h

    def forward(self, x):

        self.rnn_hidden = self.init_hidden()

        out, hidden = self.rnn(x, self.rnn_hidden)
        out = self.dropout(out)

        return out


class TagEncoder(nn.Module):

    def __init__(self, tag_emb_size, tag_nb):
        super(TagEncoder, self).__init__()

        self.tag_emb_size = tag_emb_size
        self.tag_nb = tag_nb

        # padding_idx is a part of self.nb_attr, so need to +1
        self.tag_embeddings = nn.Embedding(self.tag_nb + 1, self.tag_emb_size, padding_idx=0)
        self.merge_tag = nn.Linear(self.tag_emb_size * self.tag_nb, self.tag_emb_size)
        self.relu = nn.ReLU()

    def forward(self, x):

        enc_tag = self.tag_embeddings(x)
        enc_tag = self.relu(self.merge_tag(enc_tag.view(x.shape[0], -1)))

        return enc_tag


class Encoder(nn.Module):

    def __init__(self, char_emb_size, word_emb_size, char_rnn_hidden, word_rnn_hidden, char_vocab_size, word_vocab_size, treebank_emb_size=0, treebank_vocab_size=0, dropout_ratio=0.2,
                 wordform_type=None,  bi_char=False, rnn_type='gru', device=torch.device('cpu')):
        super(Encoder, self).__init__()

        assert not (wordform_type not in ['embed', 'pretrained'] and treebank_emb_size > 0), "Treebank embedding method only supported with embed and pretrained wordform types"

        self.char_emb_size = char_emb_size
        self.word_emb_size = word_emb_size
        self.treebank_emb_size = treebank_emb_size
        self.char_rnn_hidden = char_rnn_hidden
        self.word_rnn_hidden = word_rnn_hidden
        self.char_vocab_size = char_vocab_size
        self.word_vocab_size = word_vocab_size
        self.treebank_vocab_size = treebank_vocab_size
        self.dropout_ratio = dropout_ratio
        self.device = device
        self.rnn_type = rnn_type
        self.wordform_type = wordform_type

        if wordform_type == 'embed':
            self.wordform_embeddings = nn.Embedding(self.word_vocab_size, self.word_emb_size, padding_idx=0)
            input_size = self.char_rnn_hidden*2+self.word_emb_size
        elif wordform_type == 'pretrained':
            input_size = self.char_rnn_hidden*2 + self.word_emb_size
        elif wordform_type == 'encode':
            self.wordform_encoder = WordEncoder(self.word_emb_size, self.word_rnn_hidden, self.word_vocab_size, self.dropout_ratio, self.device, rnn_type=self.rnn_type)
            input_size = self.char_rnn_hidden*2
        else:
            input_size = self.char_rnn_hidden*2

        if self.treebank_emb_size > 0:
            self.treebank_embeddings = nn.Embedding(self.treebank_vocab_size, self.treebank_emb_size, padding_idx=0)
            input_size += treebank_emb_size

        self.word_encoder = CharEncoder(self.char_emb_size, self.char_rnn_hidden, self.char_vocab_size, self.dropout_ratio, self.device,
                                        mode='last', bi_char=bi_char, rnn_type=self.rnn_type, bidirectional=True)

        self.context_encoder = SentenceEncoder(input_size, self.word_rnn_hidden, self.dropout_ratio, self.device, rnn_type=self.rnn_type)

    def forward(self, x_char, x_word, x_treebank):

        word_emb, char_encoder_hidden, char_encoder_out = self.word_encoder((x_char[0].view(x_char[0].shape[1:]), x_char[1].view(x_char[1].shape[1:])))

        if self.wordform_type == 'embed':
            wordform_emb = self.wordform_embeddings(x_word)

            if self.treebank_emb_size > 0:
                treebank_emb = self.treebank_embeddings(x_treebank)
                word_emb = torch.cat([word_emb, wordform_emb, treebank_emb], 2)
            else :
                word_emb = torch.cat([word_emb, wordform_emb], 2)
            context_emb = self.context_encoder(word_emb)[0]
        elif self.wordform_type == 'pretrained':

            if self.treebank_emb_size > 0:
                treebank_emb = self.treebank_embeddings(x_treebank)
                word_emb = torch.cat([word_emb, x_word, treebank_emb], 2)
            else :
                word_emb = torch.cat([word_emb, x_word], 2)
            context_emb = self.context_encoder(word_emb)[0]
        elif self.wordform_type == 'encode':
            surface_context_emb = self.wordform_encoder(x_word)[0]
            char_context_emb = self.context_encoder(word_emb)[0]
            context_emb = tuple(char_context_emb, surface_context_emb)
        else:
            context_emb = self.context_encoder(word_emb)[0]

        return char_encoder_hidden, char_encoder_out, context_emb


class CharAttnDecoder(nn.Module):

    def __init__(self, to_be_decoded, char_embeddings_size, tag_embeddings_size, hidden_size, vocab, context_concat_size, max_word_len=20, max_sent_len=50, dropout_ratio=0,
                 rnn_type='gru', model_type='context-concat', attn_type='char', bi_char=False, device=torch.device('cpu')):
        super(CharAttnDecoder, self).__init__()

        assert not (rnn_type =='lstm' and 'hidden' in model_type), "LSTM is not supported with HIDDEN context type"
        assert not ('context' in attn_type and model_type != 'char-hidden'), 'Context attention is only supported with char hidden model type'

        self.bi_char = bi_char

        self.to_be_decoded = to_be_decoded
        self.rnn_type = rnn_type
        self.model_type = model_type
        self.attn_type = attn_type
        self.num_layer_rnn = 1

        self.char_embeddings_size = char_embeddings_size
        self.tag_embeddings_size = tag_embeddings_size
        self.hidden_size = hidden_size
        self.dropout_ratio = dropout_ratio
        self.max_sent_len = max_sent_len
        self.max_word_len = max_word_len

        self.vocab = vocab
        self.idx2token = {v: k for k, v in vocab.items()}
        self.vocab_size = len(vocab)

        if context_concat_size != -1:
            self.context_concat_size = context_concat_size
        else:
            self.context_concat_size = int(hidden_size * .25)

        self.char_attn = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.char_attn_combine = nn.Linear(self.hidden_size * 3, self.hidden_size)

        self.attn_input_size = self.char_embeddings_size + self.tag_embeddings_size if self.to_be_decoded == 'lemma' else self.char_embeddings_size

        self.context_attn = nn.Linear(self.attn_input_size + self.hidden_size, self.max_sent_len)
        self.context_attn_combine = nn.Linear(self.attn_input_size + self.hidden_size, self.attn_input_size)

        self.embedding = nn.Embedding(len(vocab) + 1, self.char_embeddings_size)

        if model_type == 'context-concat':
            self.W_context = nn.Linear(2 * hidden_size, self.context_concat_size)
            self.W_char = nn.Linear(2 * hidden_size, hidden_size)
            input_size = char_embeddings_size + self.context_concat_size + tag_embeddings_size if self.to_be_decoded == 'lemma' else char_embeddings_size + self.context_concat_size
        elif model_type == 'context-hidden' or model_type == 'char-hidden':
            self.W_context = nn.Linear(2 * hidden_size, hidden_size)
            self.W_char = nn.Linear(2 * hidden_size, hidden_size)
            input_size = char_embeddings_size + tag_embeddings_size if self.to_be_decoded == 'lemma' else char_embeddings_size

        if self.model_type == 'char-hidden':
            self.num_layer_rnn = 2

        if rnn_type == 'gru':
            self.rnn = nn.GRU(input_size, hidden_size, num_layers=self.num_layer_rnn, batch_first=True)
        elif rnn_type == 'lstm':
            self.rnn = nn.LSTM(input_size, hidden_size, num_layers=self.num_layer_rnn, batch_first=True)

        self.classifier = nn.Linear(hidden_size, self.vocab_size)
        self.dropout = nn.Dropout(self.dropout_ratio)
        self.relu = nn.ReLU()
        self.tanh = nn.Tanh()
        self.softmax = nn.Softmax(dim=2)

        self.device = device

    def forward_step(self, encoder_outputs, decoder_hidden, y_step, context_emb, tag_emb, attn_mask):

        # embeddings : batch * 1 * embedding_size
        embeddings = self.embedding(y_step)

        if self.model_type == 'context-concat':
            embeddings = torch.cat([embeddings, context_emb.unsqueeze(1)], 2)
        if self.to_be_decoded == 'lemma':
            embeddings = torch.cat([embeddings, tag_emb.unsqueeze(1)], 2)

        embeddings = self.dropout(embeddings)

        if self.attn_type == 'char-context' and self.model_type == 'char-hidden':

            # attn_weights : batch * max_length
            co_attn_weights = self.context_attn(torch.cat((embeddings, decoder_hidden[1].unsqueeze(1)), 2))
            co_attn_weights[~attn_mask[1]] = -10000
            co_attn_weights = self.softmax(co_attn_weights)

            # batch * 1 * hidden
            co_attn_applied = torch.bmm(co_attn_weights, encoder_outputs[1])

            # output : batch * 1 * hidden
            embeddings = torch.cat((embeddings, co_attn_applied), 2)
            embeddings = self.context_attn_combine(embeddings)
            embeddings = self.relu(embeddings)

        decoder_output, decoder_hidden = self.rnn(embeddings, decoder_hidden)

        if 'char' in self.attn_type:
            # att weights : batch * max_char_len
            ch_attn_weights = torch.bmm(decoder_output, self.char_attn(encoder_outputs[0]).transpose(1, 2)).squeeze(1)
            ch_attn_weights[~attn_mask[0]] = -100000
            ch_attn_weights = self.softmax(ch_attn_weights.unsqueeze(1))

            context = torch.bmm(ch_attn_weights, encoder_outputs[0])
            concat_input = torch.cat([context, decoder_output], -1)
            decoder_output = self.tanh(self.char_attn_combine(concat_input))

        output = self.classifier(decoder_output.squeeze(1))

        return output, decoder_hidden

    def forward(self, char_enc_out, char_seq_len, word_emb, context_emb, tag_emb, y, use_teacher_forcing):

        batch_size = context_emb.shape[0]
        char_hidden_size = char_enc_out.shape[-1]

        char_seq_len = char_seq_len.squeeze(0)

        all_decoder_outputs = Variable(torch.zeros(y[0].shape[1], batch_size, self.vocab_size)).to(self.device)

        char_attn_mask = None
        context_attn_mask = None

        char_encoder_outputs = None
        context_encoder_outputs = None

        if 'char' in self.attn_type:
            char_encoder_outputs = torch.zeros(batch_size, self.max_word_len, char_hidden_size).to(self.device)
            char_encoder_outputs[:, :char_enc_out.shape[1], :] = char_enc_out[:,:,:]
            char_attn_mask = torch.arange(self.max_word_len)[None, :] < char_seq_len[:, None].to(torch.device('cpu'))

            if 'context' in self.attn_type:
                context_attn_mask = torch.arange(self.max_sent_len)[None, :] < torch.tensor([[batch_size]])
                context_attn_mask = context_attn_mask.unsqueeze(0).expand(batch_size, *context_attn_mask.shape)
                context_encoder_outputs = torch.zeros(self.max_sent_len, self.hidden_size).to(self.device)
                context_encoder_outputs = context_encoder_outputs.unsqueeze(0).expand(batch_size, *context_encoder_outputs.shape)

        attn_mask = tuple([char_attn_mask, context_attn_mask])
        encoder_outputs = tuple([char_encoder_outputs, context_encoder_outputs])

        decoder_input = torch.tensor([2]).to(self.device)
        decoder_input = decoder_input.unsqueeze(0).expand(batch_size, *decoder_input.shape)

        if self.bi_char:
            word_emb = self.relu(self.W_char(word_emb))
        context_emb = self.relu(self.W_context(context_emb))

        if self.model_type == 'context-concat':
            if self.rnn_type == 'gru':
                decoder_hidden = word_emb.unsqueeze(0).contiguous()
            elif self.rnn_type == 'lstm':
                decoder_hidden = tuple(word_emb[0].unsqueeze(0).contiguous(),
                                  word_emb[1].unsqueeze(0).contiguous())
        elif self.model_type == 'context-hidden':
            decoder_hidden = context_emb.unsqueeze(0).contiguous()
        elif self.model_type == 'char-hidden':
            decoder_hidden = torch.cat([context_emb.unsqueeze(0),
                                        word_emb.unsqueeze(0)], 0)

        if use_teacher_forcing:
            # Teacher forcing: Feed the target as the next input
            for idx in range(y[0].shape[1]):

                decoder_output, decoder_hidden = self.forward_step(encoder_outputs,
                                                               decoder_hidden,
                                                               decoder_input,
                                                               context_emb,
                                                               tag_emb,
                                                               attn_mask)
                all_decoder_outputs[idx] = decoder_output
                decoder_input = y[0][:, idx].view(batch_size, 1)

                # detach_hidden(decoder_hidden)
        else:
            # Without teacher forcing: use its own predictions as the next input
            for idx in range(y[0].shape[1]):

                decoder_output, decoder_hidden = self.forward_step(encoder_outputs,
                                                               decoder_hidden,
                                                               decoder_input,
                                                                   context_emb,
                                                                   tag_emb,
                                                               attn_mask)
                all_decoder_outputs[idx] = decoder_output

                topv, topi = decoder_output.data.topk(1)

                decoder_input = topi.detach().long()

                # detach_hidden(decoder_hidden)
                # TODO break loop with EOS_Token

        return all_decoder_outputs

    def predict(self, char_enc_out, char_seq_len, word_emb, context_emb, tag_emb, word_idx, device, max_len=50):
        self.device = device
        predictions = []

        if self.rnn_type == 'gru':
            word_emb = word_emb[word_idx, :].unsqueeze(0)
        elif self.rnn_type == 'lstm':
            word_emb = tuple(w[word_idx, :].unsqueeze(0) for w in word_emb)

        context_emb = context_emb[word_idx, :].unsqueeze(0)

        if self.to_be_decoded == 'lemma':
            tag_emb = tag_emb[word_idx, :].unsqueeze(0)

        char_enc_out = char_enc_out[word_idx, :, :].unsqueeze(0)
        char_seq_len = char_seq_len[:, word_idx]

        batch_size = context_emb.shape[0]
        char_hidden_size = char_enc_out.shape[-1]

        char_attn_mask = None
        context_attn_mask = None

        char_encoder_outputs = None
        context_encoder_outputs = None

        if 'char' in self.attn_type:
            char_encoder_outputs = torch.zeros(batch_size, self.max_word_len, char_hidden_size).to(self.device)
            char_encoder_outputs[:, :char_enc_out.shape[1], :] = char_enc_out[:, :, :]
            char_attn_mask = torch.arange(self.max_word_len)[None, :] < char_seq_len[:, None].to(torch.device('cpu'))

            if 'context' in self.attn_type:
                context_attn_mask = torch.arange(self.max_sent_len)[None, :] < torch.tensor([[batch_size]])
                context_attn_mask = context_attn_mask.unsqueeze(0).expand(batch_size, *context_attn_mask.shape)
                context_encoder_outputs = torch.zeros(self.max_sent_len, self.hidden_size).to(self.device)
                context_encoder_outputs = context_encoder_outputs.unsqueeze(0).expand(batch_size,
                                                                                      *context_encoder_outputs.shape)

        attn_mask = tuple([char_attn_mask, context_attn_mask])
        encoder_outputs = tuple([char_encoder_outputs, context_encoder_outputs])

        decoder_input = torch.tensor([2]).to(self.device)
        decoder_input = decoder_input.unsqueeze(0).expand(batch_size, *decoder_input.shape)

        if self.bi_char:
            word_emb = self.relu(self.W_char(word_emb))
        context_emb = self.relu(self.W_context(context_emb))

        if self.model_type == 'context-concat':
            if self.rnn_type == 'gru':
                decoder_hidden = word_emb.unsqueeze(0).contiguous()
            elif self.rnn_type == 'lstm':
                decoder_hidden = tuple(word_emb[0].unsqueeze(0).contiguous(),
                                       word_emb[1].unsqueeze(0).contiguous())
        elif self.model_type == 'context-hidden':
            decoder_hidden = context_emb.unsqueeze(0).contiguous()
        elif self.model_type == 'char-hidden':
            decoder_hidden = torch.cat([context_emb.unsqueeze(0),
                                        word_emb.unsqueeze(0)], 0)

        for idx in range(max_len):
            decoder_output, decoder_hidden = self.forward_step(encoder_outputs,
                                                               decoder_hidden,
                                                               decoder_input,
                                                               context_emb,
                                                               tag_emb,
                                                               attn_mask)
            topv, topi = decoder_output.data.topk(1)
            ni = topi[0][0]

            decoder_input = Variable(torch.LongTensor([ni])).to(self.device)
            decoder_input = decoder_input.unsqueeze(0).expand(batch_size, *decoder_input.shape)

            if ni == 1:
                break
            if ni.item() > 3:
                predictions.append(self.idx2token[ni.item()])

        return predictions
