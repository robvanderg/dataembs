"""Evaluation for the SIGMORPHON 2019 shared task, task 2.
Computes various metrics on input.

"""

import argparse
import logging
import os
import numpy as np

from collections import namedtuple
from pathlib import Path

import pandas as pd

import logging
import sys


LOG_FORMAT = '%(asctime)s, %(levelname)-8s %(message)s'

LOGGER = logging.getLogger('MorphTagger')
logging.basicConfig(format=LOG_FORMAT, datefmt='%d-%m-%Y:%H:%M:%S', level=logging.INFO, stream=sys.stdout)

log = logging.getLogger(Path(__file__).stem)

COLUMNS = "ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC".split()
ConlluRow = namedtuple("ConlluRow", COLUMNS)
SEPARATOR = ";"


def distance(str1, str2):
    """Simple Levenshtein implementation."""
    m = np.zeros([len(str2)+1, len(str1)+1])
    for x in range(1, len(str2) + 1):
        m[x][0] = m[x-1][0] + 1
    for y in range(1, len(str1) + 1):
        m[0][y] = m[0][y-1] + 1
    for x in range(1, len(str2) + 1):
        for y in range(1, len(str1) + 1):
            if str1[y-1] == str2[x-1]:
                dg = 0
            else:
                dg = 1
            m[x][y] = min(m[x-1][y] + 1, m[x][y-1] + 1, m[x-1][y-1] + dg)
    return int(m[len(str2)][len(str1)])


def set_equal(str1, str2):
    set1 = set(str1.split(SEPARATOR))
    set2 = set(str2.split(SEPARATOR))
    return set1 == set2


def manipulate_data(pairs):
    log.info("Lemma acc, Lemma Levenshtein, morph acc, morph F1")

    count = 0
    lemma_acc = 0
    lemma_lev = 0
    morph_acc = 0

    f1_precision_scores = 0
    f1_precision_counts = 0
    f1_recall_scores = 0
    f1_recall_counts = 0

    for r, o in pairs:
        log.debug("{}\t{}\t{}\t{}".format(r.LEMMA, o.LEMMA, r.FEATS, o.FEATS))
        count += 1
        lemma_acc += (r.LEMMA == o.LEMMA)
        lemma_lev += distance(r.LEMMA, o.LEMMA)
        morph_acc += set_equal(r.FEATS, o.FEATS)

        r_feats = set(r.FEATS.split(SEPARATOR)) - {"_"}
        o_feats = set(o.FEATS.split(SEPARATOR)) - {"_"}

        union_size = len(r_feats & o_feats)
        reference_size = len(r_feats)
        output_size = len(o_feats)

        f1_precision_scores += union_size
        f1_recall_scores += union_size
        f1_precision_counts += output_size
        f1_recall_counts += reference_size

    f1_precision = f1_precision_scores / (f1_precision_counts or 1)
    f1_recall = f1_recall_scores / (f1_recall_counts or 1)
    f1 = 2 * (f1_precision * f1_recall) / (f1_precision + f1_recall + 1E-20)

    return (100 * lemma_acc / count, lemma_lev / count, 100 * morph_acc / count, 100 * f1)

def strip_comments(lines):
    for line in lines:
        if not line.startswith("#"):
            yield line


def read_conllu(file: Path):
    with open(file, encoding='utf-8') as f:
        yield from strip_comments(f)


def input_pairs(reference, output):
    for r, o in zip(reference, output):
        assert r.count("\t") == o.count("\t"), (r.count("\t"), o.count("\t"), o)
        if r.count("\t") > 0:
            r_conllu = ConlluRow._make(r.split("\t"))
            o_conllu = ConlluRow._make(o.split("\t"))
            yield r_conllu, o_conllu


def evaluate(gold_file, pred_file, max_length=0):
    reference = read_conllu(gold_file)
    output = read_conllu(pred_file)
    cur_results = manipulate_data(input_pairs(reference, output))

    LOGGER.info('Evaluation completed')
    LOGGER.info('Lemma Acc:{}, Lemma Lev. Dist: {}, Morph acc: {}, F1: {} '.format(*cur_results))

    return {
        #'Language': language_name,
        # 'Language Code': LANGUAGES[language_name][0],
        # 'Baseline Lemma Acc': float(LANGUAGES[language_name][1]),
        # 'Baseline Lemma Lev. Dist': float(LANGUAGES[language_name][2]),
        # 'Baseline Morph Acc': float(LANGUAGES[language_name][3]),
        # 'Baseline Morph F1': float(LANGUAGES[language_name][4]),
        'Lemma Acc': cur_results[0],
        'Lemma Lev. Dist': cur_results[1],
        'Morph Acc': cur_results[2],
        'Morph F1': cur_results[3],
    }


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('please specify gold and prediction')
    else:
        for k, v in  evaluate(sys.argv[1], sys.argv[2]).items():
            print('{}: {}'.format(k, v))

