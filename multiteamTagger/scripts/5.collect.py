import os
import sys
import numpy as np


def get_scores(scoreFile):
    if not os.path.exists(scoreFile):
        return [0.0, 0.0, 0.0, 0.0]
    scores = [0.0, 0.0, 0.0, 0.0]
    for line in open(scoreFile):
        if line.startswith('Lemma Acc'):
            scores[0] = float(line.strip().split(' ')[-1])
        if line.startswith('Lemma Lev'):
            scores[1] = float(line.strip().split(' ')[-1])
        if line.startswith('Morph Acc'):
            scores[2] = float(line.strip().split(' ')[-1])
        if line.startswith('Morph F1'):
            scores[3] = float(line.strip().split(' ')[-1])
    return scores


def get_svm_score(out_file):
    results = [l.rstrip() for l in open(out_file)]
    return float(results[23])


def print_word_count(data_dir):
    out = open('word-counts.txt', 'w')
    t2c = dict()
    trees = os.listdir(data_dir)
    for tree_dir in trees:
        files = os.listdir(os.path.join(data_dir,tree_dir))
        for f in files:
            if f.endswith('train.conllu'):
                train_file=f
                tree_name = f.split('-')[0]
                break
        c = 0
        with open(os.path.join(data_dir, tree_dir, train_file)) as f:
            for l in f:
                if not l.startswith('#'):
                    c += 1
        out.write('{} {} {}\n'.format(tree_dir, tree_name, c))
    out.close()


def get_metadata(tree2pair, word_count_file, pairs_dir, base_dataset, concat_dataset, pred_dataset, gold_dataset):

    tree2meta = dict()

    wc_file = open(word_count_file)
    lines = [l for l in wc_file]
    w2c = {l.rstrip().split()[0]: l.rstrip().split()[2] for l in lines}
    w2n = {l.rstrip().split()[0]: l.rstrip().split()[1] for l in lines}

    for tree in tree2pair:
        pair, tgt, idx, wo = tree2pair[tree]

        # family
        # word size

        ws = w2c[tree]

        base_score = get_scores(os.path.join(base_dataset, tree, 'score'))
        concat_score = get_scores(os.path.join(concat_dataset, pair, idx + '.score'))
        pred_score = get_scores(os.path.join(pred_dataset, pair, idx + '.score'))
        gold_score = get_scores(os.path.join(gold_dataset, pair, idx + '.score'))

        svm_score = get_svm_score(os.path.join(pairs_dir, pair + '.train.conllu.out'))

        tree2meta[tree] = [w2n[tree], w2n[tgt], ws, wo, base_score, concat_score, pred_score, gold_score, svm_score]

    return tree2meta


def print_table(tree2meta, scr_idx):
    table_str = ''
    table_str += '\\begin{table*}\n\\small\n\\begin{tabular}{ll|rr|rrrr|s}\n'
    table_str += 'srcData & tgtData & wordSize (K) & wordOverlap & single & concat & pred & gold & svm \\\\ \n'
    table_str += '\\hline\n'

    avg = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    for tree in tree2meta:
        meta = tree2meta[tree]
        scores = [meta[4][scr_idx], meta[5][scr_idx], meta[6][scr_idx], meta[7][scr_idx]]
        avg = avg + np.array(scores+[float(meta[-1])*100])
        max_idx = scores.index(max(scores))
        str_scores = []
        for i, s in enumerate(scores):
            if i == max_idx:
                str_scores.append('\\textbf{' +'{:.2f}'.format(s)+'}')
            else:
                str_scores.append('{:.2f}'.format(s))

        table_str += '{} & {} & {:.2f} & {:.2f} & {} & {} & {} & {} & {:.2f} \\\\ \n'.format(meta[0], meta[1], float(meta[2])/1000, float(meta[3]),
                                                                                            str_scores[0], str_scores[1], str_scores[2], str_scores[3],
                                                                                           float(meta[-1])*100)
    avg = (avg / len(tree2meta)).tolist()
    table_str += '\\hline\n'
    table_str += '{} & {} & {} & {} & {:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f} \\\\ \n'.format('', '',
                                                                                         '',
                                                                                         '',
                                                                                         avg[0], avg[1],
                                                                                         avg[2], avg[3],
                                                                                         avg[4])
    table_str += '\\end{tabular}\n'
    table_str += '\\end{table*}\n'
    table_str = table_str.replace('_','\_')
    print(table_str)


def print_raw(tree2meta, scr_idx):
    table_str = ''
    table_str += 'srcData, tgtData, wordSize(K), wordOverlap, single, concat, pred, gold, svm\n'

    avg = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    for tree in tree2meta:
        meta = tree2meta[tree]
        scores = [meta[4][scr_idx], meta[5][scr_idx], meta[6][scr_idx], meta[7][scr_idx]]
        avg = avg + np.array(scores + [float(meta[-1]) * 100])
        max_idx = scores.index(max(scores))
        table_str += '{}, {}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}\n'.format(meta[0], meta[1],
                                                                                             float(meta[2]) / 1000,
                                                                                             float(meta[3]),
                                                                                             float(scores[0]), float(scores[1]), float(scores[2]), float(scores[3]),
                                                                                             float(meta[-1]) * 100)
    avg = (avg / len(tree2meta)).tolist()
    table_str += '\\hline\n'
    table_str += '{}, {}, {}, {}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}\n'.format('', '', '', '', avg[0], avg[1],
                                                                                                 avg[2], avg[3],
                                                                                                 avg[4])
    print(table_str)


def get_pairs(pairs, model_dir):
    tree2pair = dict()
    pairs_dir = os.listdir(model_dir)
    for line in open(pairs):
        src, tgt, wo = line.rstrip().split()
        if src+'88'+tgt in pairs_dir:
            tree2pair[src] = [src+'88'+tgt, tgt, '0', wo]
        elif tgt+'88'+src in pairs_dir:
            tree2pair[src] = [tgt+'88'+src, tgt, '1', wo]
    return tree2pair


# print_word_count('../2019/task2')
tree2pair = get_pairs('pairs.txt', '../models-treebank-gold')
tree2meta = get_metadata(tree2pair, 'word-counts.txt', '../pairs', '../models-test', '../models-treebank-concat', '../models-treebank-pred', '../models-treebank-gold')
if sys.argv[1] == 'table':
    if sys.argv[2] == 'lemma':
        print_table(tree2meta, 0)
    elif sys.argv[2] == 'morph':
        print_table(tree2meta, 3)
    else:
        print('Choose either morph or lemma for result table')
elif sys.argv[1] == 'raw':
    if sys.argv[2] == 'lemma':
        print_raw(tree2meta, 0)
    elif sys.argv[2] == 'morph':
        print_raw(tree2meta, 3)
    else:
        print('Choose either morph or lemma for result table')
else:
    print('Choose either table or raw as print option')




