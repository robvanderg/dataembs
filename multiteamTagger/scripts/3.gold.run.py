import os
import sys

def getDatasets(dataset):
    train = dev = test = ""
    pairs = {}
    for conlFile in os.listdir(dataset):
        if conlFile.endswith('.train.conllu.pred'):
            k = conlFile.replace('.train.conllu.pred', '')
            train = conlFile
            if k in pairs:
                pairs[k]['train'] = os.path.join('pairs', train)
            else:
                pair = dict()
                pair['train'] = os.path.join('pairs', train)
                pairs[k] = pair
        if conlFile.endswith('.dev.conllu.pred'):
            dev = conlFile
            k = conlFile.replace('.dev.conllu.pred', '')
            if k in pairs:
                pairs[k]['dev'] = os.path.join('pairs', dev)
            else:
                pair = dict()
                pair['dev'] = os.path.join('pairs', dev)
                pairs[k] = pair
        if conlFile.endswith('.test.conllu.pred'):
            test = conlFile
            k = conlFile.replace('.test.conllu.pred', '')
            if k in pairs:
                pairs[k]['test'] = os.path.join('pairs', test)
            else:
                pair = dict()
                pair['test'] = os.path.join('pairs', test)
                pairs[k] = pair
    return pairs


pairs = getDatasets('pairs')
for k in pairs:
    #if os.path.isfile('models-test/' + dataset + '/score'):
    #    continue
    cmd = 'python3 src/tagger.py --model models-treebank-gold/' + k
    cmd += ' --train ' + pairs[k]['train']
    cmd += ' --dev ' + pairs[k]['dev']
    cmd += ' --test ' + pairs[k]['test']
    cmd += ' --train_size 500000 --treebank_emb_size 32'
    print(cmd)



