import os
import sys

def getDatasets(dataset):
    train = dev = test = ""
    pairs = {}
    for conlFile in os.listdir(dataset):
        if conlFile.endswith('.train.conllu.pred'):
            k = conlFile.replace('.train.conllu.pred', '')
            train = conlFile
            if k in pairs:
                pairs[k]['train'] = os.path.join('pairs', train)
            else:
                pair = dict()
                pair['train'] = os.path.join('pairs', train)
                pairs[k] = pair
        if conlFile.endswith('.dev.conllu.pred'):
            dev = conlFile
            k = conlFile.replace('.dev.conllu.pred', '')
            if k in pairs:
                pairs[k]['dev'] = os.path.join('pairs', dev)
            else:
                pair = dict()
                pair['dev'] = os.path.join('pairs', dev)
                pairs[k] = pair
        if conlFile.endswith('.test.conllu.pred'):
            test = conlFile
            k = conlFile.replace('.test.conllu.pred', '')
            if k in pairs:
                pairs[k]['test'] = os.path.join('pairs', test)
            else:
                pair = dict()
                pair['test'] = os.path.join('pairs', test)
                pairs[k] = pair
    return pairs


pairs = getDatasets('pairs')
gpuIdx = 0
outFiles = []
for i in range(4):
    outFiles.append(open('run.' + str(i), 'w'))

for kIdx, k in enumerate(pairs):
    outFile = outFiles[kIdx % 4]
    #if os.path.isfile('models-test/' + dataset + '/score'):
    #    continue
    outFile.write('python3 src/tagger.py --model models-treebank-gold/' + k)
    outFile.write(' --train ' + pairs[k]['train'])
    outFile.write(' --dev ' + pairs[k]['dev'])
    #cmd += ' --test ' + pairs[k]['test']
    outFile.write(' --train_size 500000 --treebank_emb_size 32 --treebank_pred')
    outFile.write(' --device ' + str(kIdx % 2 ) + '\n')

for i in range(4):
    outFiles[i].close()
