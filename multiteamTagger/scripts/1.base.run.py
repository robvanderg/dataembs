import os
import sys

def getTrainDevTest(path):
    train = dev = test = ''
    for conlFile in os.listdir(path):
        if conlFile.endswith('train.conllu'):
            train = conlFile
        if conlFile.endswith('dev.conllu'):
            dev = conlFile
        if conlFile.endswith('test.conllu'):
            test = conlFile
    return path+'/'+train, path+'/'+dev, path+'/'+test

for datasetIdx, dataset in enumerate(os.listdir('2019/task2/')):
    train, dev, test = getTrainDevTest('2019/task2/' + dataset)
    cmd = 'python3 src/tagger.py --model models-baseline/' + dataset + ' --train_size 250000 '
    cmd += ' --train ' + train + ' --dev ' + dev + ' --test ' + test
    cmd += ' --device ' + str(datasetIdx % 8)
    #outFile = open('base.run.' + str(datasetIdx % 8), 'a')
    #outFile.write(cmd + '\n')
    #outFile.close()
    print(cmd)

#
#gpuIdx = 0
#outFiles = []
#for i in range(4):
#    outFiles.append(open('run.' + str(i), 'w'))
#
#for kIdx, k in enumerate(pairs):
#    outFile = outFiles[kIdx % 4]
#    #if os.path.isfile('models-test/' + dataset + '/score'):
#    #    continue
#    outFile.write('python3 src/tagger.py --model models-treebank-gold/' + k)
#    outFile.write(' --train ' + pairs[k]['train'])
#    outFile.write(' --dev ' + pairs[k]['dev'])
#    #cmd += ' --test ' + pairs[k]['test']
#    outFile.write(' --train_size 500000 --treebank_emb_size 32 --treebank_pred')
#    outFile.write(' --device ' + str(kIdx % 2 ) + '\n')

#for i in range(4):
#    outFiles[i].close()


