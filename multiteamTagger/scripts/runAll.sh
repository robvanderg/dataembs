./scripts/0.getData.sh

python3 scripts/1.base.run.py > 1.run.sh
chmod +x 1.run.sh
./1.run.sh

python3 scripts/2.concat.run.py > 2.run.sh
chmod +x 2.run.sh
./2.run.sh

python3 scripts/3.gold.run.py > 3.run.sh
chmod +x 3.run.sh
./3.run.sh

python3 scripts/3.gold.predict.py > 3.pred.sh
chmod +x 3.pred.sh
./3.pred.sh

python3 scripts/3.gold.eval.py > 3.eval.sh
chmod +x 3.eval.sh
./3.eval.sh

python3 scripts/4.pred.run.py > 4.run.sh
chmod +x 4.run.sh
./4.run.sh

python3 scripts/5.collect.py raw lemma > raw.lemma
python3 scripts/5.collect.py raw morph > raw.morph

